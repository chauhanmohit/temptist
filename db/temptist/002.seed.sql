-- 50upy5m3Dog

INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 0, 'Birthday', null, null, true, 'cake', true;
INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 3, 'Christmas', null, null, false, 'tree', true;
INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 99, 'Custom event', null, null, true, 'gift', false;
INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 2, 'Valentine''s Day', null, null, false, 'heart01', true;
INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 1, 'Wedding Anniversary', null, null, true, '', true;
INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 4, 'Hanukkah', null, null, false, 'ico01', true;
INSERT INTO holidays (ix, title, day, month, is_user_defined, icon, is_unique) select 5, 'Chinese New Year', null, null, false, '', true;

INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'm0@temptist.com', '$2a$08$gzYqYrSUPPAX6NE9k5DVJOoy.SmtcxK/pbavVslZoBVylyyLLG6E.', 'Male of unkown age', 0, 0, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'm1@temptist.com', '$2a$08$Y0Vt77V8OyaXAN6RH.La.ubWqZfqWouGpcvCFB3DcNPpcY7DKQ1Iu', 'Male aged 17-24', 0, 1, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'm2@temptist.com', '$2a$08$ZBI74NQFqHpST7ORkpeAIuIAOT3Hvdv4TASakUIiP0yp7IKWXLQly', 'Male aged 25-30', 0, 2, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'm3@temptist.com', '$2a$08$i6Z4IGAD5mIRO1nAJ2mLO.CrGBpjyDDz3oP38UfAyUelFlTrFOWQy', 'Male aged 31-35', 0, 3, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'm4@temptist.com', '$2a$08$X26QCgumng/TE8juCqop7uPya6DozOVzOAHkuBVsQBGEN324R0Bgu', 'Male aged 36 or over', 0, 4, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'f0@temptist.com', '$2a$08$iMY83UbiQpunYMMTr51DhukZpmJa4qqIrIwWTgUiV/48XzS7ks7Xi', 'Female of unkown age', 1, 0, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'f1@temptist.com', '$2a$08$DYgjxoRxiuqq03mQYNMcseETL5yIm1V/u8g3gjf6F80yNYPuwQKvu', 'Female aged 17-24', 1, 1, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'f2@temptist.com', '$2a$08$MO8I5yOvFF5yu/fFPdGBYO5kiPh1HFN9cvJ55F.CKOCmPBiKpUQPu', 'Female aged 25-30', 1, 2, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'f3@temptist.com', '$2a$08$/pyzg3UYgcjPESjfMUCed.SbDNx8aesRAo9Z4fY5naAinlDdpRmMO', 'Female aged 31-35', 1, 3, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'f4@temptist.com', '$2a$08$CvRA2HmnHdQpeY0NcjrLs.SGBNtRUlt0J4eTNh5eh5Uv0rpV4eYlS', 'Female aged 36 or over', 1, 4, null, false, true, false, true;
INSERT INTO users (email, password, name, gender, age_range, date_of_birth, is_locked, is_special, is_admin, has_signed_up) SELECT 'francis@temptist.com', '$2a$08$hF1W.Q9d3bG4E5WrpkHlFeLWLyGd6Xz9DKZM6YpzmKZf.wvzdiqVC', 'Francis Smedley', 0, 4, null, false, null, true, true;
