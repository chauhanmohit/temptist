-- CREATE USER postgres CREATEDB PASSWORD 'root';

-- CREATE DATABASE temptist WITH ENCODING 'utf8';
-- ALTER DATABASE temptist OWNER TO postgres;
-- \c temptist 

-- User
CREATE TABLE users
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  email character varying(128),
  password character varying(64),
  name character varying(32),
  gender int,
  age_range int default 0 not null,
  date_of_birth timestamp without time zone,
  country character varying(2),
  profile_img_url character varying(2048),
  is_locked boolean default false,
  is_special boolean default false,
  is_editor boolean default false,
  is_admin boolean default false,
  has_signed_up boolean default false,
  has_verified_email boolean default false,
  has_taken_tour boolean default false,
  can_email boolean,
  metadata varchar, --can be jsonb in postgres 9.4
  facebook varchar,
  twitter varchar,
  website varchar,
  source int,
  notifications int default 0,
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users OWNER TO temptist;

-- Login
CREATE TABLE logins
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  user_id bigint,
  username character varying(128),
  token character varying(2048),  
  CONSTRAINT logins_pkey PRIMARY KEY (id),
  CONSTRAINT fk_logins_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logins OWNER TO temptist;

-- Token
CREATE TABLE tokens
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  user_id bigint,
  token character varying(128),
  CONSTRAINT tokens_pkey PRIMARY KEY (id),
  CONSTRAINT fk_tokens_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tokens OWNER TO temptist;

-- Item
CREATE TABLE items
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  user_id bigint,
  url character varying(2048),
  img_url character varying(2048),
  price_cur character varying(3),
  price_val decimal(10,2),
  CONSTRAINT items_pkey PRIMARY KEY (id),
  CONSTRAINT fk_items_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE items OWNER TO temptist;

-- Listing
CREATE TABLE listings
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  removed timestamp without time zone,
  user_id bigint,
  item_id bigint,
  title character varying(75),
  url character varying(2048),
  img_url character varying(2048),
  price_cur character varying(3),
  price_val decimal(10,2),
  comments character varying(200) default '',
  tags character varying(40) default '',
  is_favourite boolean default false,
  is_private boolean default false,
  is_removed boolean default false,
  CONSTRAINT listings_pkey PRIMARY KEY (id),
  CONSTRAINT fk_listings_item_id FOREIGN KEY (item_id)
      REFERENCES items (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_listings_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE listings OWNER TO temptist;

-- Price
CREATE TABLE prices
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  item_id bigint,
  url character varying(2048),
  price_cur character varying(3),
  price_val decimal(10,2),
  valid_from timestamp without time zone default (now() at time zone 'utc'),
  valid_to timestamp without time zone,
  CONSTRAINT prices_pkey PRIMARY KEY (id),
  CONSTRAINT fk_prices_item_id FOREIGN KEY (item_id)
      REFERENCES items (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE prices OWNER TO temptist;

-- Friendship
CREATE TABLE friendships
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  user_id bigint,
  friend_id bigint,
  nickname character varying(32),
  nudge_status int default 0,
  CONSTRAINT friendships_pkey PRIMARY KEY (id),
  CONSTRAINT fk_friendships_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_friendships_friend_id FOREIGN KEY (friend_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE friendships OWNER TO temptist;

-- Notification
CREATE TABLE notifications
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  user_id bigint,
  nudge_id bigint,
  notification_type int,
  message character varying(512),
  subject character varying(128),
  href character varying(128),
  has_been_read boolean default false,
  CONSTRAINT notifications_pkey PRIMARY KEY (id),
  CONSTRAINT fk_notifications_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_notifications_nudge_id FOREIGN KEY (nudge_id)
      REFERENCES nudges (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE notifications OWNER TO temptist;

-- Holiday
CREATE TABLE holidays
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  ix int,
  is_user_defined boolean,
  title character varying(64),
  day int,
  month int,
  question character varying(64),
  icon character varying(16),
  is_unique boolean default true,
  CONSTRAINT holidays_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE holidays OWNER TO temptist;

-- Event
CREATE TABLE events
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  user_id bigint,
  holiday_id bigint,  
  title character varying(64),
  occurs timestamp without time zone, --node-postgres doesn't like the date type as it marshalls them to js Date objects and the timezone goes awry
  next timestamp without time zone, 
  CONSTRAINT events_pkey PRIMARY KEY (id),
  CONSTRAINT fk_events_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_events_holiday_id FOREIGN KEY (holiday_id)
      REFERENCES holidays (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE events OWNER TO temptist;

-- Nudge
CREATE TABLE nudges
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  queued timestamp without time zone,
  completed timestamp without time zone,
  user_id bigint,
  event_id bigint,
  listing_id bigint,
  nudge_type int,
  subject character varying(64),
  message character varying(512),
  nudge_status int,
  CONSTRAINT nudges_pkey PRIMARY KEY (id),
  CONSTRAINT fk_nudges_user_id FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_nudges_event_id FOREIGN KEY (event_id)
      REFERENCES events (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_listings_user_id FOREIGN KEY (listing_id)
      REFERENCES listings (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE nudges OWNER TO temptist;

-- Recipient
CREATE TABLE recipients
(
  id bigserial NOT NULL,
  created timestamp without time zone default (now() at time zone 'utc'),
  nudge_id bigint,
  friendship_id bigint,
  email character varying(128),
  outcome int,
  CONSTRAINT recipients_pkey PRIMARY KEY (id),
  CONSTRAINT fk_recipients_nudge_id FOREIGN KEY (nudge_id)
      REFERENCES nudges (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_recipients_friendship_id FOREIGN KEY (friendship_id)
      REFERENCES friendships (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE recipients OWNER TO temptist;
