--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: holidays; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY holidays (id, created, ix, is_user_defined, title, day, month, question, icon, is_unique) FROM stdin;
1	2015-09-01 06:40:20.587178	0	t	Birthday	\N	\N	\N	cake	t
2	2015-09-01 06:40:20.632096	3	f	Christmas	\N	\N	\N	tree	t
3	2015-09-01 06:40:20.640413	99	t	Custom event	\N	\N	\N	gift	f
4	2015-09-01 06:40:20.648703	2	f	Valentine's Day	\N	\N	\N	heart01	t
5	2015-09-01 06:40:20.657012	1	t	Wedding Anniversary	\N	\N	\N		t
6	2015-09-01 06:40:20.665394	4	f	Hanukkah	\N	\N	\N	ico01	t
7	2015-09-01 06:40:20.673742	5	f	Chinese New Year	\N	\N	\N		t
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, created, email, password, name, gender, age_range, date_of_birth, country, profile_img_url, is_locked, is_special, is_editor, is_admin, has_signed_up, has_verified_email, has_taken_tour, can_email, metadata, facebook, twitter, website, source, notifications) FROM stdin;
1	2015-09-01 06:40:20.682202	m0@temptist.com	$2a$08$gzYqYrSUPPAX6NE9k5DVJOoy.SmtcxK/pbavVslZoBVylyyLLG6E.	Male of unkown age	0	0	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
2	2015-09-01 06:40:20.690449	m1@temptist.com	$2a$08$Y0Vt77V8OyaXAN6RH.La.ubWqZfqWouGpcvCFB3DcNPpcY7DKQ1Iu	Male aged 17-24	0	1	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
3	2015-09-01 06:40:20.698812	m2@temptist.com	$2a$08$ZBI74NQFqHpST7ORkpeAIuIAOT3Hvdv4TASakUIiP0yp7IKWXLQly	Male aged 25-30	0	2	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
4	2015-09-01 06:40:20.707135	m3@temptist.com	$2a$08$i6Z4IGAD5mIRO1nAJ2mLO.CrGBpjyDDz3oP38UfAyUelFlTrFOWQy	Male aged 31-35	0	3	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
5	2015-09-01 06:40:20.715531	m4@temptist.com	$2a$08$X26QCgumng/TE8juCqop7uPya6DozOVzOAHkuBVsQBGEN324R0Bgu	Male aged 36 or over	0	4	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
6	2015-09-01 06:40:20.723858	f0@temptist.com	$2a$08$iMY83UbiQpunYMMTr51DhukZpmJa4qqIrIwWTgUiV/48XzS7ks7Xi	Female of unkown age	1	0	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
7	2015-09-01 06:40:20.732187	f1@temptist.com	$2a$08$DYgjxoRxiuqq03mQYNMcseETL5yIm1V/u8g3gjf6F80yNYPuwQKvu	Female aged 17-24	1	1	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
8	2015-09-01 06:40:20.740521	f2@temptist.com	$2a$08$MO8I5yOvFF5yu/fFPdGBYO5kiPh1HFN9cvJ55F.CKOCmPBiKpUQPu	Female aged 25-30	1	2	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
9	2015-09-01 06:40:20.748819	f3@temptist.com	$2a$08$/pyzg3UYgcjPESjfMUCed.SbDNx8aesRAo9Z4fY5naAinlDdpRmMO	Female aged 31-35	1	3	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
10	2015-09-01 06:40:20.757112	f4@temptist.com	$2a$08$CvRA2HmnHdQpeY0NcjrLs.SGBNtRUlt0J4eTNh5eh5Uv0rpV4eYlS	Female aged 36 or over	1	4	\N	\N	\N	f	t	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
11	2015-09-01 06:40:20.765523	francis@temptist.com	$2a$08$hF1W.Q9d3bG4E5WrpkHlFeLWLyGd6Xz9DKZM6YpzmKZf.wvzdiqVC	Francis Smedley	0	4	\N	\N	\N	f	\N	f	t	t	f	f	\N	\N	\N	\N	\N	\N	0
13	2015-09-01 06:40:40.423984	guest.m1@temptist.com	$2a$08$Y0Vt77V8OyaXAN6RH.La.ubWqZfqWouGpcvCFB3DcNPpcY7DKQ1Iu	Male aged 17-24	0	1	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
14	2015-09-01 06:40:40.432299	guest.m2@temptist.com	$2a$08$ZBI74NQFqHpST7ORkpeAIuIAOT3Hvdv4TASakUIiP0yp7IKWXLQly	Male aged 25-30	0	2	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
15	2015-09-01 06:40:40.440535	guest.m3@temptist.com	$2a$08$i6Z4IGAD5mIRO1nAJ2mLO.CrGBpjyDDz3oP38UfAyUelFlTrFOWQy	Male aged 31-35	0	3	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
16	2015-09-01 06:40:40.449618	guest.m4@temptist.com	$2a$08$X26QCgumng/TE8juCqop7uPya6DozOVzOAHkuBVsQBGEN324R0Bgu	Male aged 36 or over	0	4	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
18	2015-09-01 06:40:40.466769	guest.f1@temptist.com	$2a$08$DYgjxoRxiuqq03mQYNMcseETL5yIm1V/u8g3gjf6F80yNYPuwQKvu	Female aged 17-24	1	1	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
19	2015-09-01 06:40:40.474723	guest.f2@temptist.com	$2a$08$MO8I5yOvFF5yu/fFPdGBYO5kiPh1HFN9cvJ55F.CKOCmPBiKpUQPu	Female aged 25-30	1	2	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
20	2015-09-01 06:40:40.48296	guest.f3@temptist.com	$2a$08$/pyzg3UYgcjPESjfMUCed.SbDNx8aesRAo9Z4fY5naAinlDdpRmMO	Female aged 31-35	1	3	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
21	2015-09-01 06:40:40.491466	guest.f4@temptist.com	$2a$08$CvRA2HmnHdQpeY0NcjrLs.SGBNtRUlt0J4eTNh5eh5Uv0rpV4eYlS	Female aged 36 or over	1	4	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
24	2015-09-01 10:22:58.858828	vbguest.temptist@gmail.com	$2a$08$3nO7sGCEEVR1mTKKHUSKMOoeRdQnfXzJB9T1isE50DVyLPWpiI47a	\N	\N	0	\N	false	\N	f	f	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
12	2015-09-01 06:40:40.394148	guest.m0@temptist.com	$2a$08$d03BF7ipSaZSOuOqwmdb6OxYEUF4qgmUYNpgerX7DiCMaCTeG5Ye.	Male of unkown age	0	0	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
23	2015-09-01 06:57:04.284	mss.vikrant@gmail.com	$2a$08$d03BF7ipSaZSOuOqwmdb6OxYEUF4qgmUYNpgerX7DiCMaCTeG5Ye.	vikrant	0	0	\N	false	https://temptist-silo.s3.amazonaws.com/f4e37889d5f2b418628d944d0e06439d	f	f	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
17	2015-09-01 06:40:40.458331	guest.f0@temptist.com	$2a$08$d03BF7ipSaZSOuOqwmdb6OxYEUF4qgmUYNpgerX7DiCMaCTeG5Ye.	Female of unkown age	1	0	\N	\N	\N	f	f	t	f	t	f	f	\N	\N	\N	\N	\N	\N	0
22	2015-09-01 06:45:57.857432	mss.guneet@gmail.com	$2a$08$FOLxaEjKhyfxMK9c./mnteovIP/2SVMVxzR/TK6AL9sk3w03Bn5ly	\N	1	0	\N	false	\N	f	f	f	t	t	f	f	\N	\N	\N	\N	\N	\N	0
26	2015-09-02 12:32:40.449465	mss.mohit@gmail.com	$2a$08$GGfT/lf0qhjP9GRXiHwm/.MR7BbdoTZghn.GgX23/EZUlLYxO962.	\N	0	0	\N	false	\N	f	f	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
25	2015-09-01 10:52:12.932339	guest.f@temptist.com	$2a$08$ui6suY/W4doAME2BFLxb.uCw6.qMSXU5WEyFII6E59IKmg4N64.Re	test	1	0	\N	false	\N	f	f	f	f	t	f	f	\N	\N	\N	\N	\N	\N	1
27	2015-09-02 12:59:09.478675	guest.m@temptist.com	$2a$08$27NS0GAO7.uY03rIgfm66uD6r4P6VWYXL7wZglbD4.6HmdTLWeDmi	\N	0	0	\N	false	\N	f	f	f	f	t	f	f	\N	\N	\N	\N	\N	\N	0
\.


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY events (id, created, user_id, holiday_id, title, occurs, next) FROM stdin;
\.


--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('events_id_seq', 1, false);


--
-- Data for Name: friendships; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY friendships (id, created, user_id, friend_id, nickname, nudge_status) FROM stdin;
1	2015-09-01 06:59:36.858897	22	23	name	0
2	2015-09-01 07:13:56.78383	23	22	\N	0
3	2015-09-01 10:58:09.538927	23	25	test	0
4	2015-09-01 11:12:41.891571	12	23	vikrant	0
\.


--
-- Name: friendships_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('friendships_id_seq', 4, true);


--
-- Name: holidays_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('holidays_id_seq', 7, true);


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY items (id, created, user_id, url, img_url, price_cur, price_val) FROM stdin;
1	2015-09-01 06:48:39.044958	22	http://www.amazon.co.uk/gp/product/B00KAKUN3E/ref=s9_pop_gw_g465_i2/278-7651739-0470155?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=1B2Y0PP0HD848KZARARR&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00
2	2015-09-01 06:50:07.590895	22	http://www.amazon.co.uk/gp/product/B00Z9TWIIY/ref=s9_simh_gw_p465_d0_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=05SPV3X2K6Q9XYW18B0G&pf_rd_t=36701&pf_rd_p=577047927&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00
3	2015-09-01 06:54:14.50283	22	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00
4	2015-09-01 10:25:21.263186	24	http://www.feelunique.com/p/bareMinerals-Complexion-Rescue-Tinted-Hydrating-Gel-Cream-35ml	/static/images/noimage.png	XXX	0.00
5	2015-09-01 10:53:30.677391	25	http://www.feelunique.com/p/Bare-Escentuals-id-SPF15-Matte-Foundation-Lockable-Sifter-6g	/static/images/noimage.png	XXX	0.00
6	2015-09-01 11:17:18.207834	12	http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	/static/images/noimage.png	XXX	0.00
7	2015-09-01 12:12:30.436232	22	http://www.feelunique.com/p/Real-Techniques-Setting-Brush	/static/images/noimage.png	XXX	0.00
8	2015-09-02 10:25:47.068328	25	http://www.amazon.co.uk/gp/product/B00VXMY262/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-4&pf_rd_r=0N85QQF65B7E7ZG8GR5Q&pf_rd_t=36701&pf_rd_p=577050447&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00
9	2015-09-02 11:08:03.001937	25	http://www.amazon.co.uk/first2savvv-outdoor-purple-camera-DSC-W800/dp/B00GLKNTI8/ref=pd_sim_23_4?ie=UTF8&refRID=0PG26MX3GSGVJB8SBXG8	/static/images/noimage.png	XXX	0.00
10	2015-09-02 13:04:56.45023	27	http://www.amazon.co.uk/gp/product/B00WYYCV9I/ref=s9_simh_gw_p23_d1_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-3&pf_rd_r=0EGE58G49KFFP576YMP2&pf_rd_t=36701&pf_rd_p=577048787&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00
11	2015-09-02 13:15:50.62164	27	http://www.amazon.co.uk/Amazon-W87CUN-Fire-TV-Stick/dp/B00KAKUN3E/ref=pd_bxgy_23_img_y	/static/images/noimage.png	XXX	0.00
12	2015-09-03 07:51:23.10433	25	http://www.amazon.co.uk/gp/product/B00T5M30M4?ref_=gb1h_img_m-5_b7a7_d2eec4c3&smid=A3P5ROKL5A1OLE	/static/images/noimage.png	XXX	0.00
13	2015-09-03 09:18:09.739846	22	http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	/static/images/noimage.png	XXX	0.00
14	2015-09-03 09:26:16.07663	17	https://www.amazon.co.uk/b/ref=lp_7112402031_gb1h_img_m-5_b7a7_7b6491b2?rh=i%3Aelectronics%2Cn%3A7112402031&ie=UTF8&smid=A3P5ROKL5A1OLE&node=7112402031	/static/images/noimage.png	XXX	0.00
\.


--
-- Name: items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('items_id_seq', 14, true);


--
-- Data for Name: listings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY listings (id, created, removed, user_id, item_id, title, url, img_url, price_cur, price_val, comments, tags, is_favourite, is_private, is_removed) FROM stdin;
1	2015-09-01 06:48:39.098867	\N	22	1		http://www.amazon.co.uk/gp/product/B00KAKUN3E/ref=s9_pop_gw_g465_i2/278-7651739-0470155?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=1B2Y0PP0HD848KZARARR&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00			f	f	t
2	2015-09-01 06:48:40.343	\N	22	1	Amazon Fire TV Stick	http://www.amazon.co.uk/gp/product/B00KAKUN3E/ref=s9_pop_gw_g465_i2/278-7651739-0470155?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=1B2Y0PP0HD848KZARARR&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/41khRcF1xPL._SY300_QL70_.jpg	GBP	3.49	thanks	,	t	f	f
3	2015-09-01 06:50:07.647648	\N	22	2		http://www.amazon.co.uk/gp/product/B00Z9TWIIY/ref=s9_simh_gw_p465_d0_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=05SPV3X2K6Q9XYW18B0G&pf_rd_t=36701&pf_rd_p=577047927&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00			f	f	t
24	2015-09-02 10:25:47.376	\N	25	8	SanDisk SDMX26-008G-G46K Clip Jam MP3 Player - 8 GB, Black: Amazon.co.uk: A	http://www.amazon.co.uk/gp/product/B00VXMY262/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-4&pf_rd_r=0N85QQF65B7E7ZG8GR5Q&pf_rd_t=36701&pf_rd_p=577050447&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/31a3zZNoNqL._SX300_QL70_.jpg	GBP	7.99	sandisk	sandisk	t	f	f
5	2015-09-01 06:54:14.562766	\N	22	3		http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00			f	f	t
8	2015-09-01 06:57:29.516	\N	23	3	Sony DSCW800 Compact Digital Camera - Black: Amazon.co.uk: Electronics	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/41JRFY5W6tL._SY300_QL70_.jpg	GBP	59.99	camera	camera	t	f	f
7	2015-09-01 06:57:27.397703	\N	23	3		http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00			f	f	t
6	2015-09-01 06:54:16.399	\N	22	3	Sony DSCW800 Compact Digital Camera - Black: Amazon.co.uk: Electronics	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/41JRFY5W6tL._SY300_QL70_.jpg	GBP	59.99	yuiyuyut	tyuyiy	t	t	f
4	2015-09-01 06:50:10.387	\N	22	2	Fire TV Stick Mini USB Cable. Designed to Power Your: Amazon.co.uk: Electro	http://www.amazon.co.uk/gp/product/B00Z9TWIIY/ref=s9_simh_gw_p465_d0_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=05SPV3X2K6Q9XYW18B0G&pf_rd_t=36701&pf_rd_p=577047927&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/41V7NML7e9L._SX300_QL70_.jpg	GBP	79.00	hfhghf	dfhfhh	f	f	f
9	2015-09-01 10:25:21.400937	\N	24	4		http://www.feelunique.com/p/bareMinerals-Complexion-Rescue-Tinted-Hydrating-Gel-Cream-35ml	/static/images/noimage.png	XXX	0.00			f	f	t
10	2015-09-01 10:25:22.49	\N	24	4	bareMinerals® Complexion Rescue™ Tinted Hydrating Gel Cream 35ml - feeluniq	http://www.feelunique.com/p/bareMinerals-Complexion-Rescue-Tinted-Hydrating-Gel-Cream-35ml	http://cdn1.feelunique.com/img/products/56118/bareMinerals_Complexion_Rescue_trade__Tinted_Hydrating_Gel_Cream_35ml_1429176532_main.jpg	GBP	22.00	cream	cream	t	t	f
11	2015-09-01 10:48:24.706365	\N	12	3		http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00			f	f	t
12	2015-09-01 10:48:25.831	\N	12	3	Sony DSCW800 Compact Digital Camera - Black: Amazon.co.uk: Electronics	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/41JRFY5W6tL._SY300_QL70_.jpg	GBP	6.99	gfhfh	fghgfh	t	t	f
17	2015-09-01 11:13:12.736	\N	12	3	Sony DSCW800 Compact Digital Camera - Black: Amazon.co.uk: Electronics	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/41JRFY5W6tL._SY300_QL70_.jpg	GBP	59.99		camera	f	f	f
18	2015-09-01 11:15:59.409	\N	23	5	bareMinerals® SPF15 Matte Foundation 6g - feelunique.com	http://www.feelunique.com/p/Bare-Escentuals-id-SPF15-Matte-Foundation-Lockable-Sifter-6g	http://cdn1.feelunique.com/img/products/24329/bareMinerals_SPF15_Matte_Foundation_6g_1363885356_main.jpg	GBP	21.00		gacchi	f	f	f
19	2015-09-01 11:17:18.315	\N	12	6	Rimmel Stay Matte Pressed Powder 14g - feelunique.com	http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	http://cdn1.feelunique.com/img/products/32089/Rimmel_Stay_Matte_Pressed_Powder_14g_1366299521_main.jpg	GBP	3.99	matte	matte	f	f	f
20	2015-09-01 11:20:32.131868	\N	17	6		http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	/static/images/noimage.png	XXX	0.00			f	f	t
21	2015-09-01 11:20:32.545013	\N	17	6		http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	/static/images/noimage.png	XXX	0.00			f	f	t
22	2015-09-01 11:20:33.753	\N	17	6	Rimmel Stay Matte Pressed Powder 14g - feelunique.com	http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	http://cdn1.feelunique.com/img/products/32089/Rimmel_Stay_Matte_Pressed_Powder_14g_1366299521_main.jpg	GBP	3.99	matte	matte	f	f	f
23	2015-09-01 12:12:30.585	\N	22	7	Real Techniques Setting Brush - feelunique.com	http://www.feelunique.com/p/Real-Techniques-Setting-Brush	http://cdn1.feelunique.com/img/products/39445/Real_Techniques_Setting_Brush_1366386902_main.jpg	GBP	9.99	fhgfh	gfghfh	f	f	f
25	2015-09-02 11:08:03.27	\N	25	9	New first2savvv outdoor heavy duty purple camera case: Amazon.co.uk: Camera	http://www.amazon.co.uk/first2savvv-outdoor-purple-camera-DSC-W800/dp/B00GLKNTI8/ref=pd_sim_23_4?ie=UTF8&refRID=0PG26MX3GSGVJB8SBXG8	http://ecx.images-amazon.com/images/I/41FlTdbn0VL._SY300_QL70_.jpg	GBP	64.99	purple case	purple case	f	f	f
26	2015-09-02 13:04:56.526522	\N	27	10		http://www.amazon.co.uk/gp/product/B00WYYCV9I/ref=s9_simh_gw_p23_d1_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-3&pf_rd_r=0EGE58G49KFFP576YMP2&pf_rd_t=36701&pf_rd_p=577048787&pf_rd_i=desktop	/static/images/noimage.png	XXX	0.00			f	f	t
27	2015-09-02 13:04:59.418	\N	27	10	TVPower Mini USB Power Cable for Powering Fire TV Stick: Amazon.co.uk: Elec	http://www.amazon.co.uk/gp/product/B00WYYCV9I/ref=s9_simh_gw_p23_d1_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-3&pf_rd_r=0EGE58G49KFFP576YMP2&pf_rd_t=36701&pf_rd_p=577048787&pf_rd_i=desktop	http://ecx.images-amazon.com/images/I/51ul3j2rnFL._SX300_QL70_.jpg	GBP	35.00	usb	usb	f	f	f
28	2015-09-02 13:15:50.921	\N	27	11	Amazon Fire TV Stick	http://www.amazon.co.uk/Amazon-W87CUN-Fire-TV-Stick/dp/B00KAKUN3E/ref=pd_bxgy_23_img_y	http://ecx.images-amazon.com/images/I/41khRcF1xPL._SY300_QL70_.jpg	GBP	34.99	remote	remote	f	f	f
29	2015-09-03 07:51:23.584	\N	25	12	BenQ TW526E HD Ready 720p 3200 Lumens 3D Home Entertainment Projector: Amaz	http://www.amazon.co.uk/gp/product/B00T5M30M4?ref_=gb1h_img_m-5_b7a7_d2eec4c3&smid=A3P5ROKL5A1OLE	http://ecx.images-amazon.com/images/I/41yQMensOJL._SX300_QL70_.jpg	GBP	377.33	benq	benq	f	f	f
30	2015-09-03 09:18:10.791508	\N	22	13		http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	/static/images/noimage.png	XXX	0.00			f	f	t
32	2015-09-03 09:18:15.610667	\N	22	13		http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	/static/images/noimage.png	XXX	0.00			f	f	t
31	2015-09-03 09:18:13.045	\N	22	13	Samsung BD-J5500 3D Blu-ray and DVD Player with Built-In Apps: Amazon.co.uk	http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	http://ecx.images-amazon.com/images/I/31ixZCA9xKL._SX300_QL70_.jpg	GBP	117.00	dfgdgd	dfgdfg	f	f	f
34	2015-09-03 09:19:50.118142	\N	25	13		http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	/static/images/noimage.png	XXX	0.00			f	f	t
33	2015-09-03 09:19:36.749	\N	25	13	Samsung BD-J5500 3D Blu-ray and DVD Player with Built-In Apps: Amazon.co.uk	http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	http://ecx.images-amazon.com/images/I/31ixZCA9xKL._SX300_QL70_.jpg	GBP	499.00	samsung	samsung	f	f	f
35	2015-09-03 09:25:28.585562	\N	17	13		http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	/static/images/noimage.png	XXX	0.00			f	f	t
36	2015-09-03 09:26:16.125008	\N	17	14		https://www.amazon.co.uk/b/ref=lp_7112402031_gb1h_img_m-5_b7a7_7b6491b2?rh=i%3Aelectronics%2Cn%3A7112402031&ie=UTF8&smid=A3P5ROKL5A1OLE&node=7112402031	/static/images/noimage.png	XXX	0.00			f	f	t
37	2015-09-03 09:26:17.128	\N	17	14	Amazon.co.uk: Bose £8 Off a Case Promo: Electronics & Photo	https://www.amazon.co.uk/b/ref=lp_7112402031_gb1h_img_m-5_b7a7_7b6491b2?rh=i%3Aelectronics%2Cn%3A7112402031&ie=UTF8&smid=A3P5ROKL5A1OLE&node=7112402031	https://images-na.ssl-images-amazon.com/images/G/02/gno/sprites/global-sprite_bluebeacon-32-v1._CB308131100_.png	GBP	7.99	sdfesdsd	sfsdfsf	f	f	f
\.


--
-- Name: listings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('listings_id_seq', 37, true);


--
-- Data for Name: logins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY logins (id, created, user_id, username, token) FROM stdin;
\.


--
-- Name: logins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('logins_id_seq', 1, false);


--
-- Data for Name: nudges; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY nudges (id, created, queued, completed, user_id, event_id, listing_id, nudge_type, subject, message, nudge_status) FROM stdin;
1	2015-09-01 07:01:20.249302	\N	\N	22	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
2	2015-09-01 07:07:37.075841	\N	\N	22	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
3	2015-09-01 07:09:10.23168	\N	\N	22	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
4	2015-09-01 07:10:09.826833	\N	\N	22	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
5	2015-09-01 07:11:16.353896	\N	\N	23	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
6	2015-09-01 07:15:23.33925	\N	\N	23	\N	8	2	null has dropped a hint	There's something I'd love for you to think about buying for me! Follow the link for more details!	\N
7	2015-09-01 07:15:39.402766	\N	\N	23	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
8	2015-09-01 10:59:01.403716	\N	\N	23	\N	\N	4	null would like to share their temptist with you	Some text about their temptist	\N
9	2015-09-01 11:05:28.084154	\N	\N	23	\N	8	2	null has dropped a hint	There's something I'd love for you to think about buying for me! Follow the link for more details!	\N
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY notifications (id, created, user_id, nudge_id, notification_type, message, subject, href, has_been_read) FROM stdin;
1	2015-09-01 07:10:20.18865	23	4	4	Some text about their temptist	null would like to share their temptist with you	/tempt/MQ==-bcac069f13c94d12690fff7c2ea75067	t
5	2015-09-01 10:59:15.791097	25	8	4	Some text about their temptist	null would like to share their temptist with you	/tempt/NA==-16c41467b94635314f764e0d4369887d	t
7	2015-09-01 11:05:28.142457	25	9	2	There's something I'd love for you to think about buying for me! Follow the link for more details!	null has dropped a hint	/tempt/Ng==-c4b54bf8a2cbc95f48f8828bafb3e88c	f
4	2015-09-01 07:15:44.95502	22	7	4	Some text about their temptist	null would like to share their temptist with you	/tempt/Mw==-328e71fa23b9157e11ee3c3f7818de4c	t
2	2015-09-01 07:13:56.785037	22	\N	5	null has signed-up to temptist Why not check out their temptist.	null has signed-up to temptist	/temptist/MjM=-a20a86574bc8f07d0b03dfb9a3c8afb5	t
3	2015-09-01 07:15:23.376756	22	6	2	There's something I'd love for you to think about buying for me! Follow the link for more details!	null has dropped a hint	/tempt/Mg==-3377b62b0c80d622fad0286e1ec2ee8a	t
6	2015-09-01 10:59:15.795332	22	8	4	Some text about their temptist	null would like to share their temptist with you	/tempt/NQ==-98e2d93b9ec2d2de52211aee226a9cdb	t
\.


--
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('notifications_id_seq', 7, true);


--
-- Name: nudges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('nudges_id_seq', 9, true);


--
-- Data for Name: prices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY prices (id, created, item_id, url, price_cur, price_val, valid_from, valid_to) FROM stdin;
1	2015-09-01 06:49:26.233378	1	http://www.amazon.co.uk/gp/product/B00KAKUN3E/ref=s9_pop_gw_g465_i2/278-7651739-0470155?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=1B2Y0PP0HD848KZARARR&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	GBP	3.49	2015-09-01 06:49:26.233378	\N
2	2015-09-01 06:50:21.603475	2	http://www.amazon.co.uk/gp/product/B00Z9TWIIY/ref=s9_simh_gw_p465_d0_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=05SPV3X2K6Q9XYW18B0G&pf_rd_t=36701&pf_rd_p=577047927&pf_rd_i=desktop	GBP	79.00	2015-09-01 06:50:21.603475	\N
3	2015-09-01 06:54:28.437005	3	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	GBP	59.99	2015-09-01 06:54:28.437005	\N
4	2015-09-01 06:57:37.897941	3	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	GBP	59.99	2015-09-01 06:57:37.897941	\N
5	2015-09-01 10:26:44.939188	4	http://www.feelunique.com/p/bareMinerals-Complexion-Rescue-Tinted-Hydrating-Gel-Cream-35ml	GBP	22.00	2015-09-01 10:26:44.939188	\N
6	2015-09-01 10:48:37.379481	3	http://www.amazon.co.uk/gp/product/B00IK01PJC/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-2&pf_rd_r=0WYTCEFDTG09BWX4XKB0&pf_rd_t=36701&pf_rd_p=577052007&pf_rd_i=desktop	GBP	6.99	2015-09-01 10:48:37.379481	\N
7	2015-09-01 10:53:44.178041	5	http://www.feelunique.com/p/Bare-Escentuals-id-SPF15-Matte-Foundation-Lockable-Sifter-6g	GBP	21.00	2015-09-01 10:53:44.178041	\N
8	2015-09-01 11:17:34.864611	6	http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	GBP	3.99	2015-09-01 11:17:34.864611	\N
9	2015-09-01 11:21:31.257703	6	http://www.feelunique.com/p/Rimmel-Stay-Matte-Pressed-Powder-45g	GBP	3.99	2015-09-01 11:21:31.257703	\N
10	2015-09-01 12:12:43.413372	7	http://www.feelunique.com/p/Real-Techniques-Setting-Brush	GBP	9.99	2015-09-01 12:12:43.413372	\N
11	2015-09-02 10:26:07.104241	8	http://www.amazon.co.uk/gp/product/B00VXMY262/ref=s9_ri_gw_g23_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-4&pf_rd_r=0N85QQF65B7E7ZG8GR5Q&pf_rd_t=36701&pf_rd_p=577050447&pf_rd_i=desktop	GBP	7.99	2015-09-02 10:26:07.104241	\N
12	2015-09-02 11:08:25.03566	9	http://www.amazon.co.uk/first2savvv-outdoor-purple-camera-DSC-W800/dp/B00GLKNTI8/ref=pd_sim_23_4?ie=UTF8&refRID=0PG26MX3GSGVJB8SBXG8	GBP	64.99	2015-09-02 11:08:25.03566	\N
13	2015-09-02 13:05:27.444792	10	http://www.amazon.co.uk/gp/product/B00WYYCV9I/ref=s9_simh_gw_p23_d1_i3?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=desktop-3&pf_rd_r=0EGE58G49KFFP576YMP2&pf_rd_t=36701&pf_rd_p=577048787&pf_rd_i=desktop	GBP	35.00	2015-09-02 13:05:27.444792	\N
14	2015-09-02 13:16:07.994884	11	http://www.amazon.co.uk/Amazon-W87CUN-Fire-TV-Stick/dp/B00KAKUN3E/ref=pd_bxgy_23_img_y	GBP	34.99	2015-09-02 13:16:07.994884	\N
15	2015-09-03 07:52:05.814339	12	http://www.amazon.co.uk/gp/product/B00T5M30M4?ref_=gb1h_img_m-5_b7a7_d2eec4c3&smid=A3P5ROKL5A1OLE	GBP	377.33	2015-09-03 07:52:05.814339	\N
16	2015-09-03 09:18:36.727121	13	http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	GBP	117.00	2015-09-03 09:18:36.727121	\N
17	2015-09-03 09:20:27.092062	13	http://www.amazon.co.uk/gp/product/B00U3T7N08?ref_=gb1h_img_m-5_b7a7_f51176f4&smid=A3P5ROKL5A1OLE	GBP	499.00	2015-09-03 09:20:27.092062	\N
18	2015-09-03 09:26:38.912567	14	https://www.amazon.co.uk/b/ref=lp_7112402031_gb1h_img_m-5_b7a7_7b6491b2?rh=i%3Aelectronics%2Cn%3A7112402031&ie=UTF8&smid=A3P5ROKL5A1OLE&node=7112402031	GBP	7.99	2015-09-03 09:26:38.912567	\N
\.


--
-- Name: prices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('prices_id_seq', 18, true);


--
-- Data for Name: recipients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY recipients (id, created, nudge_id, friendship_id, email, outcome) FROM stdin;
1	2015-09-01 07:10:20.158901	4	1	mss.vikrant@gmail.com	\N
2	2015-09-01 07:15:23.351638	6	2	mss.guneet@gmail.com	\N
3	2015-09-01 07:15:44.92173	7	2	mss.guneet@gmail.com	\N
4	2015-09-01 10:59:15.655426	8	3	guest.f@temptist.com	\N
5	2015-09-01 10:59:15.657884	8	2	mss.guneet@gmail.com	\N
6	2015-09-01 11:05:28.122056	9	3	guest.f@temptist.com	\N
\.


--
-- Name: recipients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recipients_id_seq', 6, true);


--
-- Data for Name: schemaversion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schemaversion (version, name, md5) FROM stdin;
0		
\.


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tokens (id, created, user_id, token) FROM stdin;
\.


--
-- Name: tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tokens_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 27, true);


--
-- PostgreSQL database dump complete
--

