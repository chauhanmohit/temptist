var config     	= require('../../config/db'),
	knex		= require('knex'),
	bookshelf	= require('bookshelf'),
	bcrypt		= require('bcrypt')
	country     = require('../../config/country');
	
var bs = bookshelf(knex(config));

bs.plugin('registry');

var Login = bs.Model.extend({
	tableName: 'logins',
	user: function() {
		return this.belongsTo(User);
	}
});

var Logins = bs.Collection.extend({
    model: Login
});

var Token = bs.Model.extend({
	tableName: 'tokens',
	user: function() {
		return this.belongsTo(User);
	}
});

var Tokens = bs.Collection.extend({
    model: Token
});

var User = bs.Model.extend({
	tableName: 'users',
	logins: function() {
		return this.hasMany(Login);
	},
	listings: function() {
		return this.hasMany(Listing);
	},
	setPassword: function(password) {
	    this.set('password', bcrypt.hashSync(password, bcrypt.genSaltSync(8), null));
	},
	authenticate: function(password) {
	    return bcrypt.compareSync(password, this.get('password'));
	},
	toJSON: function() {
		var attr = this.attributes;
		if(attr.country) {
			attr.country = country.findByCode( attr.country );
		}
		if(! attr.profile_img_url || attr.profile_img_url == '') {
			attr.profile_img_url = '/static/images/icon-no-profile-image.svg';
		} else if(!/^https?:\/\//i.test(attr.profile_img_url)) {
			attr.profile_img_url = '/upload/' + attr.profile_img_url;
		}
		attr.nickname = attr.name ? attr.name.split(' ')[0] : '';
		//if(attr.metadata) attr.metadata = JSON.parse(attr.metadata);
		delete attr.password;
		return attr;
	}
});

console.log("User>>>>>>>>>>>>>>>>>>>>model" , User);

var Users = bs.Collection.extend({
    model: User
});

var Item = bs.Model.extend({
	tableName: 'items',
	creator: function() {
		return this.belongsTo(User);
	},
	toJSON: function() {
		var attr = this.attributes;
		return attr;
	}
});

var Items = bs.Collection.extend({
    model: Item
});

var Listing = bs.Model.extend({
	tableName: 'listings',
	user: function() {
		return this.belongsTo(User);
	},
	item: function() {
		return this.belongsTo(Item);
	},
	toJSON: function() {
		var attr = this.attributes;
		attr['item'] = this.related('item').toJSON();
		// TODO allow Tempting items to set this to false
		attr.can_give = true;
		if(attr.price_cur != 'XXX') {
			attr['price'] = { symbol: Currencies[attr.price_cur], amount: toPrice(attr.price_val) };
		}
		return attr;
	}
});

function toPrice(val) {
	if(!val || isNaN(val))
		return '0';
	return Number(val).toFixed(2).toLocaleString().replace(/\.00$/g, '');
};

var Currencies = { 'XXX': '', 'GBP': '£', 'USD': '$' };

var Listings = bs.Collection.extend({
	model: Listing
});

var Price = bs.Model.extend({
	tableName: 'prices',
	item: function() {
		return this.belongsTo(Item);
	}
});

var Prices = bs.Collection.extend({
	model: Price
});

var Friendship = bs.Model.extend({
	tableName: 'friendships',
	user: function() {
		return this.belongsTo(User);
	},
	friend: function() {
		return this.belongsTo(User, 'friend_id');
	},
	toJSON: function() {
		var attr = this.attributes;
		var friend = this.related('friend');
		var user = this.related('user');
		if(friend)
			attr['friend'] = friend.toJSON();
		if(user)
			attr['user'] = user.toJSON();
		return attr;
	}
});

var Friendships = bs.Collection.extend({
	model: Friendship
});

var Notification = bs.Model.extend({
	tableName: 'notifications',
	user: function() {
		return this.belongsTo(User);
	}
});

var Notifications = bs.Collection.extend({
	model: Notification
});

var Holiday = bs.Model.extend({
	tableName: 'holidays',
	toJSON: function() {
		return this.attributes;
	}
});

var Holidays = bs.Collection.extend({
	model: Holiday
});

var Event = bs.Model.extend({
	tableName: 'events',
	user: function() {
		return this.belongsTo(User);
	},
	holiday: function() {
		return this.belongsTo(Holiday);
	},
	toJSON: function() {
		var attr = this.attributes;
		attr['holiday'] = this.related('holiday').toJSON();
		return attr;
	}
});

var Events = bs.Collection.extend({
	model: Event
});

var Nudge = bs.Model.extend({
	tableName: 'nudges',
	user: function() {
		return this.belongsTo(User);
	},
	event: function() {
		return this.belongsTo(Event);
	},
	listing: function() {
		return this.belongsTo(Listing);
	},
	toJSON: function() {
		var attr = this.attributes;
		var user = this.related('user');
		if( user ) attr['user'] = user.toJSON();
		var listing = this.related('listing');
		if( listing ) attr['listing'] = listing.toJSON();
		var event = this.related('event');
		if( event ) attr['event'] = event.toJSON();
		return attr;
	}
});

var Nudges = bs.Collection.extend({
	model: Nudge
});

var Recipient = bs.Model.extend({
	tableName: 'recipients',
	nudge: function() {
		return this.belongsTo(Nudge);
	},
	friendship: function() {
		return this.belongsTo(Friendship);
	},
	toJSON: function() {
		var attr = this.attributes;
		var nudge = this.related('nudge');
		if( nudge ) attr['nudge'] = nudge.toJSON();
		var friendship = this.related('friendship');
		if( friendship ) attr['friendship'] = friendship.toJSON();
		return attr;
	}
});

var Recipients = bs.Collection.extend({
	model: Recipient
});

var toJSON = function(collectionOfModels) {
	var json = [];
	if(collectionOfModels && collectionOfModels.models) {
		for(var ix = 0; ix < collectionOfModels.models.length; ix++) {
			var model = collectionOfModels.models[ix];
			json.push(model.toJSON());
		}
	}
	return json;
};

module.exports = {
    Login: bs.model('Login', Login),
    Logins: bs.model('Logins', Logins),
    Token: bs.model('Token', Token),
    Tokens: bs.model('Tokens', Tokens),
    Item: bs.model('Item', Item),
    Items: bs.model('Items', Items),
    Listing: bs.model('Listing', Listing),
    Listings: bs.model('Listings', Listings),
    Price: bs.model('Price', Price),
    Prices: bs.model('Prices', Prices),
    Friendship: bs.model('Friendship', Friendship),
    Friendships: bs.model('Friendships', Friendships),
    Notification: bs.model('Notification', Notification),
    Notifications: bs.model('Notifications', Notifications),
    Holiday: bs.model('Holiday', Holiday),
    Holidays: bs.model('Holidays', Holidays),
    Event: bs.model('Event', Event),
    Events: bs.model('Events', Events),
    Nudge: bs.model('Nudge', Nudge),
    Nudges: bs.model('Nudges', Nudges),
    Recipient: bs.model('Recipient', Recipient),
    Recipients: bs.model('Recipients', Recipients),
    User: bs.model('User', User),
    Users: bs.collection('Users', Users),
    Knex: bs.knex,
    toJSON: toJSON
};