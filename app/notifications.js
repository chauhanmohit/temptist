// Messages regarding Temptists shared

Summary: [X] shared their Temptist with you…
Full Message: [X] shared their Temptist with you. You should check it out. [add link if possible]

// Hints received

Summary: [X] dropped a hint your way…
Full message: the fully message of the original hint email

// Tempts received

Summary: [X] tried to tempt you…
Full message: the fully message of the original tempt email

// Announces that a person the user invited has joined

Summary: [X] has signed-up to temptist…
Full message: [X] has signed-up to temptist.  Why not check out their Temptist. [add link if possible]

// Informs the user that someone added them to their list of friends and that someone is identified by name

Summary: [X] just added you as a friend on temptist…
Full message: as above

// Reminds the user that they added an item from their smartphone and invites them to check that the details are correct

Summary: You recently added an item using your smartphone. Check the details here…
Full message: as above but include a link to the item if possible

// Repeat of emails sent to you by temptist four and two weeks before your special dates

Summary: this should be the subject of the email from temptist
Full message: this should be the text of the message from temptist