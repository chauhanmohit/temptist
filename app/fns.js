/*************************
 *                       *
 *    T E M P T I S T    * 
 *    ---------------    *
 *   UTILITY FUNCTIONS   *
 *                       *
 *************************/

var crypto = require('crypto');

var nacl = {
		'temptist'		: ['JUXfgk2n','7XJP3pC2','phRpYzc6'],
		'nudge'			: ['jbSPTA6A','GcSCVkKX','m79XCxHG'],
		'pwreset'		: ['uett4rKH','ZLvQEEKu','HeYPAPH8'],
		'filename'		: ['RfYzjB8a','8JFZaq3P','JbbpDmbJ'],
		'verify'		: ['Qp4MPws9','wrPNfYbj','8Ws34EfN'],
		'invite'		: ['H9q8nPUS','DNm9SgBe','RLACBP54'],
		'tempt'			: ['wPZq8Mzr','jqzYWFUR','XcQ43N5T'],
		'unsubscribe'	: ['yuezCmmG','mn7aJdaF','C5XuAk9d']
	};

var hash = function(key, a, b) {
	var salt = nacl[key];
	if(salt && salt.length == 3) {
		var plaintext = salt[0] + a + salt[1] + b + salt[2];
		return crypto.createHash('md5').update(plaintext).digest('hex');
	}
	throw new Error('NACL key not found ['+key+']');
};

var idAndHash = function(tokenParam) {
	var parts = tokenParam.split('-', 2);
	if(parts.length == 2) {
		var id = new Buffer(parts[0], 'base64').toString('ascii');
		if(isNormalInteger(id))
			return { id: id, hash: parts[1] };
	}
	throw new Error('Invalid token ['+tokenParam+']');
};

var toUrl = function(key, id, other) {
	return (new Buffer(id).toString('base64')) + '-' + hash(key, id, other);
};

var toUrlAB = function(key, id, a, b) {
	return (new Buffer(id).toString('base64')) + '-' + hash(key, a, b);
};

var recentHour = function(hoursAgo) {
	var time = new Date();
	time.setHours(time.getHours() - hoursAgo);
	return '' + time.getFullYear() + time.getMonth() + time.getDate() + time.getHours();
}

var hashedFilename = function(a) {
	var time = new Date();
	return hash('filename', a, time.getTime());
}

var isEmail = function(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

var isNormalInteger = function(str) {
    var n = ~~Number(str);
    return String(n) === str && n >= 0;
};

var pushIfAbsent = function(arr, val) {
	if(!inArr(arr, val))
		arr.push(val);
};

var inArr = function(arr, search) {
    var len = arr.length;
    while( len-- ) {
        if(arr[len] === search)
           return true;
    }
};

var toSet = function(mixed, validationFn) {
	var arr = [];
	if(mixed) {
		if(mixed.constructor === Array) {
			for(var ix = 0; ix < mixed.length; ix++) {
				var val = mixed[ix].trim();
				if(validationFn(val) && arr.indexOf(val) == -1)
					arr.push(val);
			}
		} else {
			var val = mixed.trim();
			if(validationFn(val) && arr.indexOf(val) == -1)
				arr.push(val);
		}
	}
	return arr;
};

var dateFormat = function(date, fstr, utc) {
	  utc = utc ? 'getUTC' : 'get';
	  return fstr.replace (/%[YmdHMS]/g, function (m) {
	    switch (m) {
	    case '%Y': return date[utc + 'FullYear'] (); // no leading zeros required
	    case '%m': m = 1 + date[utc + 'Month'] (); break;
	    case '%d': m = date[utc + 'Date'] (); break;
	    case '%H': m = date[utc + 'Hours'] (); break;
	    case '%M': m = date[utc + 'Minutes'] (); break;
	    case '%S': m = date[utc + 'Seconds'] (); break;
	    default: return m.slice (1); // unknown code, remove %
	    }
	    // add leading zero if required
	    return ('0' + m).slice (-2);
	  });
};

var longDateFormat = function(date) {
	var str = '';
	var m = date.getUTCMonth();
	switch(m) {
	case 0: str = 'January '; break;
	case 1: str = 'February '; break;
	case 2: str = 'March '; break;
	case 3: str = 'April '; break;
	case 4: str = 'May '; break;
	case 5: str = 'June '; break;
	case 6: str = 'July '; break;
	case 7: str = 'August '; break;
	case 8: str = 'September '; break;
	case 9: str = 'October'; break;
	case 10: str = 'November '; break;
	default: str = 'December ';
	}
	var d = date.getUTCDate();
	str += d;
	switch(d) {
	case 1: str += 'st '; break;
	case 21: str += 'st '; break;
	case 31: str += 'st '; break;
	case 2: str += 'nd '; break;
	case 22: str += 'nd '; break;
	default: str += 'th ';
	}
	str += date.getUTCFullYear();
	return str;
};

var parseDate = function(dt) { // dt must be "dd/mm/yyyy"
	var b = dt.split('/');
	console.log(b);
	if(b.length != 3) return false;
	var days = ~~Number(b[0]);
	var months = ~~Number(b[1]) - 1;
	var years = ~~Number(b[2]);
	var date = new Date();
	date.setDate(days);
	date.setMonth(months);
	date.setFullYear(years);
	return date;
};

var parseAgeRange = function(dob) {
	var age = getAge(dob);
	if(age < 17)
		return 0;
	else if(age < 25)
		return 1;
	else if(age < 31)
		return 2;
	else if(age < 35)
		return 3;
	else
		return 4;
};

var getAge = function(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
};

/* nudges & notifications
 * 
 * 1: invite
 * 2. hint
 * 3. tempt
 * 4. share for event
 * 5. added you as friend (notification only)
 * 6. friend signed up (notification only)
 * 7. smartphone reminder (notification only)
 *  
 *  */
var getIcon1 = function(nudge_type) {
	switch(nudge_type) {
		case 2: return 'icon-flash';
		case 3: return 'icon-send2';
		//case 4: return ''; can't be specific
		case 5: return 'icon-user';
		case 6: return 'icon-user';
		default: return 'icon-arrow-right';
	}
};
var getIcon2 = function(nudge_type) {
	switch(nudge_type) {
		case 2: return 'ico-1';
		case 3: return 'send';
		//case 4: return ''; can't be specific
		//case 5: return 'icon-user';
		//case 6: return 'icon-user';
		default: return '';
	}
};

//var createS3Policy = function( mimetype, callback ) {
//	var s3PolicyBase64, _date, _s3Policy;
//	_date = new Date();
//	s3Policy = {
//	    "expiration": "" + (_date.getFullYear()) + "-" + (_date.getMonth() + 1) + "-" + (_date.getDate()) + "T" + (_date.getHours() + 1) + ":" + (_date.getMinutes()) + ":" + (_date.getSeconds()) + "Z",
//	    "conditions": [
//	      { "bucket": "bucketName" }, 
//	      ["starts-with", "$Content-Disposition", ""], 
//	      ["starts-with", "$key", "someFilePrefix_"], 
//	      { "acl": "public-read" }, 
//	      { "success_action_redirect": "http://example.com/uploadsuccess" }, 
//	      ["content-length-range", 0, 2147483648], 
//	      ["eq", "$Content-Type", mimetype]
//	    ]
//	};
//	s3Credentials = {
//		    s3PolicyBase64: new Buffer( JSON.stringify( s3Policy ) ).toString( 'base64' ),
//		    s3Signature: crypto.createHmac( "sha1", "yourAWSsecretkey" ).update( s3Policy ).digest( "base64" ),
//		    s3Key: "Your AWS Key",
//		    s3Redirect: "http://example.com/uploadsuccess",
//		    s3Policy: s3Policy
//	};
//	callback( s3Credentials );
//};

module.exports = {
		hash: hash,
		idAndHash: idAndHash,	
		isNormalInteger: isNormalInteger,
		pushIfAbsent: pushIfAbsent,
		isEmail: isEmail,
		toSet: toSet,
		longDateFormat: longDateFormat,
		dateFormat: dateFormat,
		parseDate: parseDate,
		toUrl: toUrl,
		toUrlAB: toUrlAB,
		recentHour: recentHour,
		hashedFilename: hashedFilename,
		parseAgeRange: parseAgeRange,
		getAge: getAge,
		getIcon1: getIcon1,
		getIcon2: getIcon2
};