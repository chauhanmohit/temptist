﻿/*************************
 *                       *
 *    T E M P T I S T    * 
 *    ---------------    *
 *    WEB/URL ROUTING    *
 *                       *
 *************************/

var async = require('asyncawait/async');
var await = require('asyncawait/await');
var Promise = require('bluebird');
Promise.onPossiblyUnhandledRejection(function(error){
    throw error;
}); // defer to Express
var aws = require('aws-sdk');
var fns = require('./fns');
var temptist = require('./temptist');
var sanitizer = require('sanitizer');
var striptags = require('striptags');
var util = require('util');
var questions = require('../config/questions');
var domain = require('../app/model/domain');
module.exports = function(app, passport) {

	// =====================================
	// EXPRESS MIDDLEWARE ==================
	// =====================================
	
	// global route to add authed user to page (if exists) - only for non-ajax requests
	app.use(function(req, res, next) {
		if(req.isAuthenticated() && !req.xhr) {
			res.locals.authed = { 
					id: req.user.id,
					name: req.user.get('name'), 
					notifications: req.user.get('notifications'),
					profile_img_url: formatProfileImgUrl( req.user.get('profile_img_url') ), 
					url: fns.toUrl('temptist', req.user.id, req.user.get('email')),
					items: [] // must be populated by route handler (using temptist.privateItems) if required by ejs view
			};
		} else
			res.locals.authed = false;
		next();
	});
	
	// =====================================
	// SITE ROOT ===========================
	// =====================================
	
	app.get('/', function(req, res) {	
		// if user is authenticated in the session, show their temptist, otherwise show the splash page 
		if (req.isAuthenticated()){
			temptist.loadTemptor(req.user).then(function(temptor) {
				gender=req.user.get('gender');
				console.log('temptor>>>>>>>>>>>>>>',temptor);
				res.locals.temptor = temptor;
				
				var fPrivateTemptist = temptist.privateTemptist(req.user);
				var fTemptingItems = temptist.temptingItemsFor(req.user, temptor);
				var ffTemptingItems = temptist.temptingItemsForGuest(req.user, gender);
				Promise.join(fPrivateTemptist, fTemptingItems,ffTemptingItems, function(listings, temptings,guesttemptings) {
				    console.log("listings>>>>",guesttemptings);
					res.render('authed/home.ejs', { 
						temptist: listings,
						temptings: temptings,
						guesttemptings:guesttemptings
					});
				});
				
				
			});
		} else
			//res.render('public/launchrock.ejs');	// <-- switch ~~~ two ~~~ to ~ live
			res.render('public/index.ejs'); 		// <--  ~~~~ these ~ lines ~ go ~~
	});
	app.get('/guest', function(req, res) {
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/tour.ejs');
	});
	app.post('/', function(req, res) {
		var email = req.body.email;
		if(fns.isEmail(email)) {
			temptist.emailContact(email, 'Launchrock page', 'This user signed up on the launchrock page');
		}
		res.locals.thanks = true;
		res.render('public/launchrock.ejs');
	});
	
	// =====================================
	// PUBLIC TEMPTIST BY TOKEN ============
	// =====================================	
	
	// example using async/await
	app.get('/temptist/:token', async(function(req, res) {
		var token = fns.idAndHash(req.params.token);
		var user = await(temptist.loadTemptistUser(token.id));
		var listings = await(temptist.publicTemptist(user));
		if(token.hash == fns.hash('temptist', user.id, user.email)) {
		    domain.User.where({ id:token.id }).fetch().then(function(fetched) {
			console.log("USER DATA>>>>>>>>>>>>>>>>>>>>>>",fetched);
			
		    
		 
		    temptist.loadTemptor(fetched).then(function(temptor) {
				gender=fetched.get('gender');
				console.log('temptor>>>>>>>>>>>>>>',temptor);
				res.locals.temptor = temptor;
				
				var fPrivateTemptist = temptist.privateTemptist(fetched);
				var fTemptingItems = temptist.temptingItemsFor(fetched, temptor);
				var ffTemptingItems = temptist.temptingItemsForGuest(fetched, gender);
				Promise.join(listings, fTemptingItems,ffTemptingItems, function(listings, temptings,guesttemptings) {
				    console.log("listings********************>>>>",listings);
				    
					res.render('authed/away.ejs', {
						temptee: user ,
						temptist: listings,
						temptings: temptings,
						guesttemptings:guesttemptings,
						
					});
				});
				
				
			});
			
			
		    });
		} else
			res.status(404).send('Not found');
	}));
		
	// =====================================
	// UNSUBSCRIBE BY TOKEN ================
	// =====================================	
	
	app.get('/unsubscribe/:token', async(function(req, res) {
		var token = fns.idAndHash(req.params.token);
		var recipient = await(temptist.loadRecipient(token.id));
		if(token.hash == fns.hash('unsubscribe', recipient.id, recipient.email)) {
			temptist.updateOutcome(recipient, 1); // 1 == unsubscribed
			temptist.updateNudgeStatus(recipient.friendship.id, 1);
			res.render('public/unsubscribe.ejs', { recipient: recipient });
		} else
			res.status(404).send('Not found');
	}));
	
	// =====================================
	// FOLLOW NUDGE EMAIL LINK =============
	// =====================================	
	
	app.get('/tempt/:token', async(function(req, res) {
		var token = fns.idAndHash(req.params.token);
		var recipient = await(temptist.loadRecipient(token.id));
		if(token.hash == fns.hash('nudge', recipient.id, recipient.email)) {
			temptist.updateOutcome(recipient, 2); // 2 == followed
			var user = await(temptist.loadUser(recipient.friendship.friend_id));
			var friend = await(temptist.loadUser(recipient.nudge.user_id));
			// follow the nudge
			if(recipient.nudge.nudge_type == 1) {
				// invitation - are they signed up?
				if(user.has_signed_up) {
					// yes, create reciprocal friendship and fwd to away temptist
					temptist.createFriendship(user, friend, friend.name);
					res.redirect('/temptist/' + fns.toUrl('temptist', friend.id, friend.email) );
				} else {
					// no, drop them into the signup process
					res.redirect('/signup?e='+ new Buffer(user.email).toString('base64'));
				}
			} else if(recipient.nudge.nudge_type == 2) {
				// hint - show temptist with dropped hint
				res.redirect('/temptist/' + fns.toUrl('temptist', friend.id, friend.email) + '?hint=' + recipient.nudge.listing_id);
			} else if(recipient.nudge.nudge_type == 3) {
				// tempt - not sure
				res.redirect('/temptist/' + fns.toUrl('temptist', friend.id, friend.email) + '?tempt=' + recipient.nudge.listing_id);
			} else if(recipient.nudge.nudge_type == 4) {
				// event share - show temptist
				res.redirect('/temptist/' + fns.toUrl('temptist', friend.id, friend.email) );
			} else
				res.status(404).send('Not found');
			
		} else
			res.status(404).send('Not found');
	}));
	
	// =====================================
	// STATIC CONTENT PAGES ================
	// =====================================
	
	app.get('/tour', function(req, res) {
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/tour.ejs');
	});

	app.get('/help', function(req, res) {
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/help.ejs');
	});
	
	app.get('/contact', function(req, res) {
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/contact.ejs');
	});

	app.post('/contact', function(req, res) {
		
		req.checkBody('umail', 'A valid email is required').isValidEmail();
		req.checkBody('ufname', 'Your name is required').len(1, 50);
		req.checkBody('feedback', 'Please write some feedback').notEmpty();
		
		var errors = req.validationErrors();
		
		if(errors) {
			res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/contact.ejs', { errors: errors } );
		}
		
		temptist.emailContact(req.param('umail'), req.param('ufname'), req.param('feedback'));
		
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/contact.ejs', { message: 'Thank you!'});
	});
	
	
	app.get('/legal', function(req, res) {
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/legal.ejs');
	});

	app.get('/about', function(req, res) {
		res.render( (req.isAuthenticated() ? 'authed' : 'public') + '/about.ejs');
	});	
		
	app.get('/guestitem', function(req, res) {
		res.render('guestitem.ejs');
	});
	// =====================================
	// LOGOUT ==============================
	// =====================================
	
	app.get('/logout', function(req, res) {
		res.clearCookie('remember_me');
		req.logout();
		res.redirect('/');
	});
	
	// =====================================
	// LOGIN ===============================
	// =====================================
	
	// show the login form
	app.get('/login', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('public/login.ejs', { message: req.flash('loginMessage') }); 
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/',
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));
	
	// =====================================
	// SIGNUP ==============================
	// =====================================
	
	// show the signup form
	app.get('/signup', function(req, res) { //console.log("hi>>>>>>>>>>>>>>>>>" , req.body); alert("heloo");
		if(req.query['e']) {
			res.locals.email = new Buffer(req.param('e'), 'base64').toString('ascii');
		}
		res.render('public/signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', { //console.log("hi>>>>>>>>>>>>>>>>>post" , req.body); alert("ikbal khan....");
		successRedirect : '/verify', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));
	
	// initialise facebook login
	app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

	// handle the callback after facebook has authenticated the user
	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			successRedirect : '/',
			failureRedirect : '/'
	}));

	
	// =====================================
	// FORGOT PASSWORD =====================
	// =====================================
	
	// show the forgot password form
	app.get('/forgot-password', function(req, res) {
		res.render('public/forgot-password.ejs', { message: '' });
	});
	
	// process the forgot password form
	app.post('/forgot-password', function(req, res) {
		var email = req.body.email;
		temptist.loadTemptistUserByEmail(email).then(function(user) {
			temptist.emailPasswordReset(user);
			res.render('public/forgot-password.ejs', { message: 'Success: ' + user.email });
		});
		// TODO catch --> res.render('public/forgot-password.ejs', { message: 'We were unable to send an email to ' + email });
	});
	
	// show the reset password form
	app.get('/reset-password/:token', validateToken, function(req, res) {
		res.render('public/reset-password.ejs', { token: req.params.token });
	});
	
	// process the reset password form
	app.post('/reset-password/:token', validateToken, function(req, res) {
		temptist.updatePassword(req.tmpuser.id, req.body.password);
		res.redirect('/login');
	});

	// =====================================
	// VERIFY AND WELCOME ==================
	// =====================================
	app.get('/verify', isLoggedIn, function(req, res) {
		temptist.loadTemptistUser(req.user.id).then(function(user) {
			temptist.emailWelcome(user);			
			res.redirect('/welcome');	
		});
	});
	
	app.get('/verify/:token', async(function(req, res) {
		var token = fns.idAndHash(req.params.token);
		var user = await(temptist.loadTemptistUser(token.id));
		if(token.hash == fns.hash('verify', user.id, user.email)) {
			temptist.verifyEmail(user);
			if(req.isAuthenticated())
				res.redirect('/');
			else
				res.redirect('/login');
			res.render('public/unsubscribe.ejs', { recipient: recipient });
		} else
			res.status(404).send('Not found');
	}));
	
	app.get('/welcome', isLoggedIn, function(req, res) {
		res.render('public/welcome.ejs');
	});
	
	// =====================================
	// HOLIDAYS & EVENTS ===================
	// =====================================
	
	// show form with user's events and all holidays
	app.get('/events', isLoggedIn, function(req, res) {
		var fHolidays = temptist.loadHolidays();
		var fEvents = temptist.loadEvents(req.user);
		
		Promise.join(fHolidays, fEvents, function(holidays, events) {
			res.render('authed/events.ejs', {
				holidays : holidays,
				events : events
			});
		});
	});
	
	// show form with user's events and all holidays
	app.get('/occasions', isLoggedIn, function(req, res) {
		var fHolidays = temptist.loadHolidays();
		var fEvents = temptist.loadEvents(req.user);
		
		Promise.join(fHolidays, fEvents, function(holidays, events) {
			res.render('authed/occasions.ejs', {
				holidays : holidays,
				events : events
			});
		});
	});
	
	// process request to add an event
	app.post('/event/add/:id', isLoggedIn, function(req, res) {
		// TODO need unique check
		temptist.loadHoliday(req.param('id')).then(function(holiday) {
			var occurs = new Date();
			occurs.setMonth(holiday.month-1);
			occurs.setDate(holiday.day);
			if(holiday.is_user_defined) {
				occurs = fns.parseDate(req.body.date);
			}
			var title = req.body.title ? req.body.title : holiday.title;
			temptist.addEvent(holiday.id, req.user.id, title, occurs).then(function(saved) {
				updateDOB(saved, req.user);
				res.send({ event: saved.toJSON() });
			});
		});
	});
	
	// update the user's date of birth, if applicable - HACK: uses magic ID!!!
	function updateDOB(event, user) {
		if(event.attributes.holiday_id == 1 && event.attributes.occurs) {
			user.set('date_of_birth', event.attributes.occurs);
			// update age range
			user.set('age_range', fns.parseAgeRange(event.attributes.occurs) );
			user.save();
		}
	};
	
	app.get('/event/edit/:id', isLoggedIn, function(req, res) {
		temptist.loadEvent(req.param('id'), req.user).then(function(event) {
			res.render('authed/event.ejs', { event: event });
		});
	});
	
	app.post('/event/edit/:id', isLoggedIn, function(req, res) {
		temptist.loadEventForUpdate(req.param('id'), req.user).then(function(event) {
			if(event) {
				if(req.body.title)
					event.set('title', req.body.title);
				if(req.body.date) {
					occurs = fns.parseDate(req.body.date);
					var oc = new Date(Date.UTC(occurs.getFullYear(), occurs.getMonth(), occurs.getDate(), 0, 0, 0));
					var nt = temptist.getNextOccurs(oc);
					event.set('occurs', oc);
					event.set('next', nt);
				}
				event.save().then(function(saved) {
					updateDOB(saved, req.user);
				});
			}
		}).then(function() {
			res.send({ result: 'okay' });
		});
	});

	app.post('/event/delete/:id', isLoggedIn, function(req, res) {
		temptist.deleteEvent(req.param('id'), req.user).then(function(event) {
			res.send({ result: 'okay' });
		});
	});
	
	// =====================================
	// FIND FRIENDS ========================
	// =====================================
	
	// show list of potential friends who are already on temptist
	app.get('/find-friends', isLoggedIn, function(req, res) {
		temptist.loadPotentialFriends(req.user).then(function(friendships) {
			res.render('authed/find-friends.ejs', {
				friends : friendships
			});
		});
	});

	// handle post of friends: existing temptist users and collection of email addresses
	app.post('/find-friends', isLoggedIn, function(req, res) {
		processFriendsForm(req);
		res.redirect('/start');
	});

	// implementation of: handle post of friends
	function processFriendsForm(req) {
		var toCheck = [];
		if(req.body.friend) {
			if( Object.prototype.toString.call( req.body.friend ) === '[object Array]' ) {
				toCheck = req.body.friend;
			} else {
				toCheck.push( req.body.friend );
			}
		}
		var friends = [];
		for(var ix = 0; ix < toCheck.length; ix++) {
			var f = toCheck[ix].split('@', 3);
			if(f.length == 3) {
				var e = f[0] + '@' + f[1];
				var n = f[2];
				if(fns.isEmail(e) && n.length > 0)
					friends.push( { email: e, nickname: n } );
			}
		}
		if (fns.isEmail(req.body.email) && req.body.nickname.length > 0)
			friends.push( { email: req.body.email, nickname: req.body.nickname } );

		var friendships = fns.toSet(req.body.friendship, fns.isNormalInteger);

		if (friends.length > 0)
			temptist.createFriendships(req.user, friends);

		if (friendships.length > 0)
			temptist.createReciprocalFriendships(req.user, friendships);
	};
	
	// =====================================
	// GET STARTED =========================
	// =====================================
	
	// how getting started options screen
	app.get('/start', isLoggedIn, function(req, res) {
		res.render('authed/start.ejs', {
			
		});
	});

	app.get('/suggest', isLoggedIn, function(req, res) {
		res.redirect('/suggest/1');
	});
	
	// show first question
	app.get('/suggest/:ix', isLoggedIn, function(req, res) {
		
		var ix = parseInt(req.param('ix'), 10);
		ix = (ix && ix > 0 && ix < 10) ? ix : 1;
		
		// load 5 items from the same demographic as the user
		temptist.loadSuggestions(req.user, ix).then(function(suggestions) {
			var q = questions.loadQuestion(req.user.get('gender'), ix-1, req.user.get('age_range'));
			res.render('authed/question.ejs', {
				listings: suggestions.listings, question: q,
				next: (ix+1), count: suggestions.next_count
			});
		});
	});	
	
	// =====================================
	// PROFILE =============================
	// =====================================
	
	// show profile page
	app.get('/profile', isLoggedIn, function(req, res) {
		req.user.load('logins').then(function(model) {
			res.render('authed/profile.ejs', {
				user: model.toJSON(),
				logins: model.related('logins')
			});	
		});
	});

	// TODO handle update of details
	app.post('/profile', isLoggedIn, function(req, res) {
		req.user.load('logins').then(function(model) {
			res.render('authed/profile.ejs', {
				user: model,
				logins: model.related('logins')
			});	
		});
	});

	// upload picture form
	app.get('/picture', isLoggedIn, function(req, res) {
		res.render('authed/picture.ejs', {
			user: req.user.toJSON()
		});	
	});	

	// handle picture upload - TODO save to S3
	app.post('/picture', isLoggedIn, function(req, res) {
		if(req.files) {
			req.user.set('profile_img_url', req.files.uploaded_img.name);
			req.user.save();
		}
		res.redirect('/picture');
	});	

	// s3 save picture to user account
	app.post('/s3picture', isLoggedIn, function(req, res) {
		if(req.body.avatar_url) {
			req.user.set('profile_img_url', req.body.avatar_url);
			req.user.save();
		}
		res.redirect('/picture');
	});	
	
	// TODO remove from this file
	var S3_BUCKET = 'temptist-silo';
	// temptist-website-s3-user
	var AWS_ACCESS_KEY = 'AKIAIQOFHWWKTJUD65HQ';
	var AWS_SECRET_KEY = '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';
	
	// s3 upload - generate signature for direct upload
	app.get('/s3sign', isLoggedIn, function(req, res){
	    aws.config.update({accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY});
	    var s3filename = fns.hashedFilename(req.user.id);
	    var s3 = new aws.S3();
	    var s3_params = {
	        Bucket: S3_BUCKET,
	        Key: s3filename,
	        Expires: 60,
	        ContentType: req.query.s3_object_type,
	        ACL: 'public-read'
	    };
	    s3.getSignedUrl('putObject', s3_params, function(err, data){
	        if(err){
	            console.log(err);
	        }
	        else{
	            var return_data = {
	                signed_request: data,
	                url: 'https://'+S3_BUCKET+'.s3.amazonaws.com/'+s3filename
	            };
	            res.write(JSON.stringify(return_data));
	            res.end();
	        }
	    });
	});
	// NB this requires a CORS configuration to be added to the s3 bucket:
	//	<?xml version="1.0" encoding="UTF-8"?>
	//	<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
	//	   <CORSRule>
	//	        <AllowedOrigin>*</AllowedOrigin>
	//	        <AllowedMethod>GET</AllowedMethod>
	//	        <AllowedMethod>POST</AllowedMethod>
	//	        <AllowedMethod>PUT</AllowedMethod>
	//	        <AllowedHeader>*</AllowedHeader>
	//	    </CORSRule>
	//	</CORSConfiguration>
	// see http://codeartists.com/post/36892733572/how-to-directly-upload-files-to-amazon-s3-from-your
	
	// =====================================
	// FRIENDS =============================
	// =====================================
	
	// load the friends page
	app.get('/friends', isLoggedIn, function(req, res) {
		temptist.loadFriendships(req.user).then(function(friendships) {
			if(friendships.length > 0) {
				res.render('authed/friends.ejs', {
					friendships: temptist.parseFriendships(friendships)
				});
			} else {
				res.redirect('/invite-friends');
			}
		});
	});
	
	// load potential friends who are already temptist users, and facebook friends
	app.get('/invite-friends', isLoggedIn, function(req, res) {
		temptist.loadPotentialFriends(req.user).then(function(friendships) {
			res.render('authed/invite-friends.ejs', {
				friends : friendships
			});
		});
	});

	// handle post of friends: existing temptist users and collection of email addresses
	app.post('/invite-friends', isLoggedIn, function(req, res) {
		processFriendsForm(req);
		res.redirect('/friends');
	});
	
	// handle new friend form: a single name and email address
	// TODO perhaps move to ajax
	app.post('/add-friend', isLoggedIn, function(req, res) {
		
		req.checkBody('email', 'A valid email is required').isValidEmail();
		req.checkBody('name', 'Your friend\'s name is required').len(1, 50);
		
		var errors = req.validationErrors();
		
		if(errors) {
			var errorText = '';
			for(var ix = 0; ix < errors.length; ix++)
				errorText += '&' + errors[ix].param +  '=bad';
			res.redirect('/friends?' + errorText);
		}
			
		var name = req.param('name');
		var email = req.param('email');
		
		// Should probably be synchronous
		temptist.createFriendshipByEmailAndNickname(req.user, email, name).then(function() {
			res.redirect('/friends');
		});
	});
	
	// =====================================
	// ADD/EDIT LISTING ====================
	// =====================================
	
	// initiate add an item
	app.get('/add', isLoggedIn, function(req, res) {
		if(req.accepts('html')) {
			if(req.xhr)
				res.render('authed/add-ajax.ejs');
			else
				res.render('authed/add.ejs', {  } );
		} else
			res.send({ result: 'okay' });
	});

	// process add a listing
	app.post('/add', isLoggedIn, function(req, res) {
		var url = req.body.url;
		
		var strict = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
		var loose = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
		
		if(! url.match(strict) && url.match(loose) )
			url = 'http://' + url;
		
		if(url.match(strict)) {
			temptist.loadOrCreateItem(url, req.user)
				.then(temptist.createListing)
				.then(temptist.crawl)
				.then(function(result) {
					if(req.accepts('html')) {
						var page = req.xhr ? 'edit-ajax' : 'edit';
						res.render('authed/' + page +'.ejs', { listing: result.listing.toJSON(), title: result.title, imgs: result.imgs, prices: result.prices, decimals: result.decimals } );
					} else
						res.send({ listing: result.listing.toJSON(), title: result.title, imgs: result.imgs, prices: result.prices, decimals: result.decimals });
				});
		}
	});
	
	// edit an existing listing
	app.get('/edit/:id', isLoggedIn, function(req, res) {
		temptist.loadListing(req.param("id"), req.user).then(function(listing) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'edit-ajax' : 'edit';
				res.render('authed/' + page +'.ejs', { listing: listing, title: null, imgs: [], prices: [], decimals: [] } );
			} else
				res.send({ listing: listing, title: null, imgs: [], prices: [], decimals: [] });
		});
	});
	

	// handle saving of an existing listing
	app.post('/edit/:id', isLoggedIn, function(req, res) {
		
		var id = req.param("id");
		var title = req.body.title.substring(0,100);
		var comments = req.body.comments.substring(0,200);
		var tags = req.body.tags.substring(0,50);
		var is_favourite = req.body.is_favourite ? true : false;
		var is_private = req.body.is_private ? true : false;
		var price_val = req.body.price_val;
		var price_cur = req.body.price_cur;
		// HACK: problems with no price selection
		if(price_cur != 'GBP' && price_cur != 'USD')
			price_cur = 'XXX';
		//if(req.body.price && req.body.price != '') {
		//	price_val = req.body.price.substring(1);
		//	price_cur = req.body.price.substring(0, 1) == '£' ? 'GBP' : 'USD';
		//}
		var val = parseFloat(price_val).toFixed(2);
		var img_url = req.body.img_url;
		
		temptist.updateListing(id, req.user, {
			title: title,
			comments: comments,
			tags: tags,
			is_favourite: is_favourite,
			is_private: is_private,
			price_cur: price_cur,
			price_val: val,
			img_url: img_url
		}).then(function(listing) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'edit-ajax' : 'edit';
				res.render('authed/' + page +'.ejs', { listing: listing, title: null, imgs: [], prices: [], decimals: [] } );
			} else
				res.send({ listing: listing, title: null, imgs: [], prices: [], decimals: [] });
		});
	});

	app.post('/remove/:id', isLoggedIn, function(req, res) {
		temptist.deleteListing(req.param('id'), req.user).then(function(event) {
			res.send({ result: 'okay' });
		});
	});
	
	// =====================================
	// DROP A HINT =========================
	// =====================================
	
	// show hint form
	app.get('/drop-a-hint/:id', isLoggedIn, function(req, res) {
		var fListing = temptist.loadListing(req.param("id"), req.user);
		var fFriends = temptist.loadFriendshipsForNudge(req.user);
		Promise.join(fListing, fFriends, function(listing, friendships) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'hint-ajax' : 'hint';
				res.render('authed/'+page+'.ejs', { listing: listing, friendships: friendships } );
			} else
				res.send({ result: 'okay' });
		});
	});
	
	// handle the hint submission
	app.post('/drop-a-hint/:id', isLoggedIn, function(req, res) {
		
		req.checkBody('message', 'You must supply a message (up to 500 characters)').len(1, 500);
		req.checkBody('friend', 'You must select a friend').isInt();
		var errors = req.validationErrors();
		
		if(errors) {
			res.send({ result: 'error', errors: errors });
		} else {
			
			var fListing = temptist.loadListing(req.param("id"), req.user);
			var fFriendship = temptist.loadFriendship(req.param("friend"), req.user);
			
			Promise.join(fListing, fFriendship, function(listing, friendship) {
				temptist.emailHint(listing, friendship, striptags(req.param('message')));
			});

			res.send({ result: 'okay' });
		}	
	});
	
	// =====================================
	// TEMPT A FRIEND ======================
	// =====================================
	
	// show tempt form
	app.get('/tempt-a-friend/:id', isLoggedIn, function(req, res) {
		var fListing = temptist.loadPublicListing(req.param("id"));
		var fFriends = temptist.loadFriendshipsForNudge(req.user);
		Promise.join(fListing, fFriends, function(listing, friendships) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'tempt-ajax' : 'tempt';
				res.render('authed/'+page+'.ejs', { listing: listing, friendships: friendships } );
			} else
				res.send({ result: 'okay' });
		});
	});
	
	// handle the tempt submission
	app.post('/tempt-a-friend/:id', isLoggedIn, function(req, res) {
		
		req.checkBody('message', 'You must supply a message (up to 500 characters)').len(1, 500);
		req.checkBody('friend', 'You must select a friend').isInt();
		var errors = req.validationErrors();
		
		if(errors) {
			res.send({ result: 'error', errors: errors });
		} else {
			
			var fListing = temptist.loadPublicListing(req.param("id"));
			var fFriendship = temptist.loadFriendship(req.param("friend"), req.user);
			
			Promise.join(fListing, fFriendship, function(listing, friendship) {
				temptist.emailTempt(listing, friendship, striptags(req.param('message')));
			});

			res.send({ result: 'okay' });
		}		
		
	});
	// =====================================
	// ADD TO MY TEMPTIST ==================
	// =====================================

	// show add form
	app.get('/add-to-mine/:id', isLoggedIn, function(req, res) {
		temptist.copyPublicListing(req.param("id"), req.user).then(function(listing) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'edit-ajax' : 'edit';
				res.render('authed/' + page +'.ejs', { listing: listing, title: null, imgs: [], prices: [], decimals: [] } );
			} else
				res.send({ result: 'okay' });
		});
	});
	
//	// handle the add submission
//	app.post('/add-to-mine/:id', isLoggedIn, function(req, res) {
//		temptist.loadPublicListing(req.param("id")).then(function(listing) {
//			if(req.accepts('html'))
//				res.render('authed/hint.ejs', { listing: listing } );
//			else
//				res.send({ result: 'okay' });
//		});
//	});

	app.post('/add-to-my/:id', isLoggedIn, function(req, res) {
		temptist.copyListing(req.param("id"), req.user).then(function(listing) {
			res.send({ result: 'okay' });
		});
	});
	
	// =====================================
	// INVITE FRIEND =======================
	// =====================================
	
	// show hint form
	app.get('/invite/:id', isLoggedIn, function(req, res) {
		temptist.loadFriendship(req.param("id"), req.user).then(function(friendship) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'invite-ajax' : 'invite';
				res.render('authed/'+page+'.ejs', { friendship: friendship } );
			} else
				res.send({ result: 'okay' });
		});
	});

	app.post('/invite/:id', isLoggedIn, function(req, res) {
		
		req.checkBody('message', 'You must supply a message (up to 500 characters)').len(1, 500);
		var errors = req.validationErrors();
		
		if(errors) {
			res.send({ result: 'error', errors: errors });
		} else {
			temptist.loadFriendship(req.param("id"), req.user).then(function(friendship) {
				temptist.emailInvite(friendship, striptags(req.param('message')));
			});
			res.send({ result: 'okay' });
		}
	});
	
	// =====================================
	// SHARE MY TEMPTIST ===================
	// =====================================
	
	// show share form for a new nudge 
	app.get('/share', isLoggedIn, function(req, res) {
		temptist.loadEvents(req.user).then(function(events) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'share-ajax' : 'share';
				res.render('authed/'+page+'.ejs', { from: req.user.get('name'), events: events } );
			} else
				res.send({ result: 'okay' });
		});
	});
	
	app.post('/share-event', isLoggedIn, async(function(req, res) {
		
		req.sanitize('event').toInt();
		
		var ent = parseInt(req.param('event'), 10);
		var event = null;
		
		var subject = req.user.get('name') + ' would like to share their temptist with you';
		var message = 'Some text about their temptist';
		
		if(ent > 0) {
			event = await(temptist.loadEvent(ent, req.user));
			subject = 'It\'s ' + event.title + ' time for ' + req.user.get('name');
			message = 'Help them celebrate';
		}
		
		temptist.createNudge(req.user, 4, event, null, subject, message).then(function(nudge) {
			if(event) nudge.event = event;
			if(req.accepts('html')) {
				var page = req.xhr ? 'nudge-ajax' : 'nudge';
				res.render('authed/'+page+'.ejs', { nudge: nudge, message: nudge.message.split('\n') } );
			} else
				res.send({ result: 'okay' });
		});
		
	}));
	
	app.post('/share-friends/:id', isLoggedIn, function(req, res) {
		temptist.loadNudge(req.param('id'), req.user).then(function(nudge) {
			// check nudge status
			
			temptist.loadFriendshipsForNudge(req.user).then(function(friendships) {
				if(req.accepts('html')) {
					var page = req.xhr ? 'recipients-ajax' : 'recipients';
					res.render('authed/'+page+'.ejs', { nudge: nudge, friendships: friendships } );
				} else
					res.send({ result: 'okay' });
			});
		});
	});
	
	app.post('/share-send/:id', isLoggedIn, function(req, res) {
		
		var friendships = fns.toSet(req.body.friendship, fns.isNormalInteger);
		
		if(friendships.length == 0) {
			res.send({ result: 'error', errors: [ { msg: 'You must select some friends' } ] });
		} else {
		
			temptist.loadNudge(req.param('id'), req.user).then(function(nudge) {
				// check nudge status

				temptist.sendNudgeToFriends(nudge, friendships);
				
				res.send({ result: 'okay' });
			});
		}
	});
	
	// show share form for an existing nudge 
	app.get('/share/:token', isLoggedIn, function(req, res) {
		if(req.accepts('html'))
			res.render('authed/share.ejs', {  } );
		else
			res.send({ result: 'okay' });
	});
	
	// handle the share submission
	app.post('/share/:token', isLoggedIn, function(req, res) {
		if(req.accepts('html'))
			res.render('authed/share.ejs', {  } );
		else
			res.send({ result: 'okay' });
	});	
	
	// =====================================
	// NOTIFICATIONS =======================
	// =====================================
	
	// notification popup
	app.get('/notifications-unread', isLoggedIn, function(req, res) {
		temptist.loadNotificationsUnread(req.user).then(function(notifications) {
			if(req.accepts('html')) {
				var page = req.xhr ? 'notifications-ajax' : 'notifications';
				res.render('authed/' + page +'.ejs', { notifications: notifications } );
			} else
				res.send({ notifications: notifications });
		});
	});
	
	// show notifications
	app.get('/notifications', isLoggedIn, function(req, res) {
		temptist.loadNotifications(req.user).then(function(notifications) {
			// format to be by day
			var days = groupByDay(notifications);
			if(req.accepts('html')) {
				var page = req.xhr ? 'notifications-ajax' : 'notifications';
				res.render('authed/' + page +'.ejs', { notifications: notifications, days: days } );
			} else
				res.send({ notifications: notifications, days: days });
			temptist.readNotifications(req.user);
		});
	});

	// read notification
	app.get('/notification-read', isLoggedIn, function(req, res) {
		temptist.readNotification(req.user, req.param('id'));
		res.send({ success: true });
	});

	
	function groupByDay(notifications) {
		var days = [];
		var day = {};
		var dt = '';
		for(var ix = 0; ix < notifications.length; ix++) {
			var ndt = fns.longDateFormat(notifications[ix].created);
			if( dt != ndt ) {
				if(day.nfs && day.nfs.length)
					days.push({ rd: day.rd, dt: day.dt, nfs: day.nfs });
				day = { rd: notifications[ix].created, dt: ndt, nfs: [] };
				dt = ndt;
			}
			day.nfs.push(notifications[ix]);
		}
		if(day.nfs && day.nfs.length)
			days.push({ rd: day.rd, dt: day.dt, nfs: day.nfs });
		//console.log(util.inspect(days, {showHidden: false, depth: null}));
		return days;
	};
	
	// =====================================
	// UTILITY FUNCTIONS ===================
	// =====================================
	
	function formatProfileImgUrl(url) {
		if(! url || url == '') {
			url = '/static/images/icon-no-profile-image.svg';
		} else if(!/^https?:\/\//i.test(url)) {
			url = '/upload/' + url;
		}
		return url;
	};
	
	// route middleware to test if a user is logged in
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on 
		if (req.isAuthenticated())
			return next();
		// if they aren't redirect them to the home page or return a 401
		if(req.xhr)
			res.status(401).send('Authentication required');
		else
			res.redirect('/');
	};

	// route middleware to test if a user is logged in with administrator permissions
	function isAdmin(req, res, next) {
		// if user is authenticated in the session and is an admin, carry on 
		if (req.isAuthenticated() && req.user.get('is_admin'))
			return next();
		// if they aren't redirect them to the home page
		res.redirect('/');
	};

	function validateToken(req, res, next) {
		var idAndHash = fns.idAndHash(req.params.token);
		temptist.loadTemptistUser(idAndHash.id).then(function(user) {
			for(var ix = 0; ix < 2; ix++) {
				if(idAndHash.hash == fns.hash('pwreset', user.email, fns.recentHour(ix))) {
					req.tmpuser = user;
					return next();
				}
			}
			return Promise.reject(new Error('Not found'));
		});
	};
		
	// ===================================================================================================================
	//
	// Admin site
	//
	// =================
	//
	// Users
	app.get('/admin/', isAdmin, function(req, res) {
		domain.Users.query('where', 'is_locked', '=', 'false')
		  .fetch()
		  .then(function(collection) {
			  res.render('admin/index.ejs', {
				  users: collection.models
			  });
		  });
	});

	// =================
	//
	// Listings
	app.get('/admin/items', isAdmin, function(req, res) {
		domain.Knex('prices').whereRaw('id in (select max(id) from prices group by url, item_id )')
			.orderBy('item_id')
			.then(function(collection) {
				res.render('admin/items.ejs', {
					listings: collection
				});
		});
	});
	
	// =====================================
	// 404 =================================
	// =====================================
	
	// NB This must be the last route defined as it will match ANYTHING
	// The theory is that if we've reached this point in the file and nothing
	// else has handled the request (i.e. the whatever the request was has not
	// been found) then we should respond with a 404. Capice?
	
	app.use(function(req, res, next){
		  res.status(404);

		  // respond with html page
		  if (req.accepts('html')) {
		    res.render('public/404', { url: req.url });
		    return;
		  }

		  // respond with json
		  if (req.accepts('json')) {
		    res.send({ error: 'Not found' });
		    return;
		  }

		  // default to plain-text. send()
		  res.type('txt').send('Not found');
		});
		app.post('/editdetail',isAdmin, function(req, res) {
	
	    
	});
	
};