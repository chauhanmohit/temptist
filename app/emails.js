var fns = require('./fns');
//Sending emails
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
// Okay so this is nuts. In order to use the aws-sdk-ses you have to supply your ROOT AWS credentials and NOT those
// of the IAM user you've specifically created with only permission to send emails. WFT???
var transporter = nodemailer.createTransport(smtpTransport({
	"host": "email-smtp.eu-west-1.amazonaws.com",
	"port": 465,
	"secure": true,
    auth: {
        user: "AKIAIQ6LPFJSLL7OSLCQ", 
        pass: "AhC7Kkms13mpLN0SBIMxetHEE/Nx5L+71ihtZnaGXfWO"
    }
}));

var gmail = nodemailer.createTransport(smtpTransport({
	"host": " smtp.gmail.com",
	"port": 465,
	"secure": true,
    auth: {
        user: "team@temptist.com", 
        pass: "fiustew2576"
    }
}));

var devMode = (process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'development');

var hostname = process.env.PARAM1 ? process.env.PARAM1 : 'http://temptist.hypergiant.co.uk'; // 'http://d.cubud.com'; // 'http://temptist.hypergiant.co.uk'; // 'https://www.temptist.com';

var HEADER = '<div style="margin-left:auto;margin-right:auto;max-width:500px;min-width:300px;border-top:2px solid #43bfa1;border-bottom:2px solid #43bfa1;">' +
  '<div style="padding:20px 30px">' +
  '<div style="padding:10px 0 25px 0;border-bottom:1px solid #ccc;">' +
  '<table width="100%" tableborder="0"><tr><td><a href="'+hostname+'"><img src="https://temptist-silo.s3.amazonaws.com/t.gif" border="0"/></a></td>' +
  '<td align="right"><a href="'+hostname+'"><img src="https://temptist-silo.s3.amazonaws.com/st.gif" border="0"/></a> ' +
  '<a href="'+hostname+'"><img src="https://temptist-silo.s3.amazonaws.com/sf.gif" border="0"/></a> ' +
  '<a href="'+hostname+'"><img src="https://temptist-silo.s3.amazonaws.com/sg.gif" border="0"/></a> ' +
  '<a href="'+hostname+'"><img src="https://temptist-silo.s3.amazonaws.com/si.gif" border="0"/></a></td></tr></table></div>';

var FOOTER = '<div style="padding:25px 0 10px 0;border-top:1px solid #ccc;margin-top:40px;">' + 
    '<p style="font-family:Arial,sans-serif;font-size:0.8em;color:#aaa">You received this email because you\'re a registered <a href="'+hostname+'" style="color:#2a806c;">temptist.com</a> user.</p>' +
    // '<p style="font-family:Arial,sans-serif;font-size:0.8em;color:#aaa">temptist<br/>419 Upper Richmond Road<br/>London, SW15 5QX<br/>United Kingdom</p>' +
    '<p style="font-family:Arial,sans-serif;font-size:0.8em;color:#aaa"><a style="color:#2a806c;" href="__unsub__">Unsubscribe from these emails</a></p></div></div></div>';

var H1 = '<h1 style="font-family:Arial,sans-serif;font-size:2em;color:#2a806c;margin-top:25px;">';

var P = '<p style="font-family:Arial,sans-serif;font-size:1em;color:#091321">';

var A = '<a style="color:#2a806c"';

function Email() {
    var self = (this);
    self.text = '';
    self.html = '';
    self.subj = '';
    self.unsub = 'mailto:team@temptist.com?subject=Unsubscribe';
    
    this.subject = function(txt) {
    	self.subj = txt;
    },
    this.h1 = function(txt) {
    	self.text += (txt + "\n\n"); 
    	self.html += (H1 + txt + '</h1>');
    },
    this.p = function(txt) {
    	self.text += (txt + "\n\n"); 
    	self.html += (P + txt + '</p>');
    },
    this.a = function(txt, url) {
    	self.text += (txt + ': ' + url + "\n\n"); 
    	self.html += (P + A + ' href="' + url + '">' + txt + '</a></p>');
    },
    this.unsubscribe = function(id, key) {
    	self.unsub = hostname+'/unsubscribe/' + fns.toUrl('unsubscribe', id, key);
    },
    this.render = function() {
    	return {
    		subject: self.subj,
    		text: self.text + '\n\n------------------------------------\nUnsubscribe: ' + self.unsub,
    		html: HEADER + self.html + FOOTER.replace('__unsub__', self.unsub)
    	};
    },
    this.sendTo = function(to, use_gmail) {
    	use_gmail = typeof b !== 'undefined' ? use_gmail : false;
    	var m = self.render();
    	if(devMode) {
    		console.log('DEV MODE EMAIL TO: ' + to + '\n\n' + m.text + '\n\n'+ m.html);
    		return;
    	}
    	var mailerTransport = use_gmail ? gmail : transporter;
    	mailerTransport.sendMail({
		    from: '"Temptist" <team@temptist.com>',
		    to: to,
		    subject: m.subject,
		    text: m.text,
		    html: m.html
		}, function(error, info) {
		    if(error){
		        console.log(error);
		    } else {
		        console.log('Message sent: ' + info.response);
		    }
		});
    }
};

var welcome = function(id, key, recipient) {
	var email = new Email();
	email.subject('Welcome to temptist!');
	email.h1('Welcome to temptist!');
	email.p('Dear ' + recipient);
	email.p('Thank you for signing up to temptist. Please verify your email address by clicking the link below.');
	email.a('Verify my email', hostname+'/verify/' + fns.toUrl('verify', id, key));
	email.p('Thanks,');
	email.p('The Temptist Team');
	//email.unsubscribe(id, key); you can't unsub from account related emails
	return email;
};

var password = function(id, key, recipient) {
	var email = new Email();
	email.subject('Request to reset your temptist password');
	email.h1('Forgotten password?');
	email.p('Dear ' + recipient);
	email.p('If you would like to reset your password please click the link below.');	
	email.a('Reset my password', hostname+'/reset-password/' + fns.toUrlAB('pwreset', id, key, fns.recentHour(0)));
	email.p('Thanks,');
	email.p('The Temptist Team');
	//email.unsubscribe(id, key); you can't unsub from account related emails
	return email;
};

var contact = function( key, name, feedback) {
	var email = new Email();
	email.subject('Feedback from temptist website');
	email.p('Name: ' + name);
	email.p('Email: ' + key);
	email.p(feedback);
	return email;
};

var nudge = function( recipient, subject, message, listing, event ) {
	var email = new Email();
	email.subject(subject);
	email.p(message);
	email.a('View on temptist', hostname+'/tempt/' + fns.toUrl('nudge', recipient.id, recipient.email));
	email.unsubscribe(recipient.id, recipient.email);
	return email;
};

module.exports = {
	welcome: welcome,
	password: password,
	contact: contact,
	nudge: nudge
};
