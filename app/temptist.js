/*************************
 *                       *
 *    T E M P T I S T    * 
 *    ---------------    *
 *     SERVICE LAYER     *
 *                       *
 *************************/

// Domain classes
var domain = require('./model/domain');
// Temptist utility functions
var fns = require('./fns');
// Promises are used for method chaining and error handling
var Promise = require('bluebird');
Promise.onPossiblyUnhandledRejection(function(error){
    throw error;
}); // throw to Express
// Request allows us to grab web pages from other websites
var request = Promise.promisify(require("request"));
// Parser to parse other web pages from other websites
var htmlparser = require("htmlparser2");
// Util fns for object inspection
var util = require('util');
// URL parsing and formatting
var liburl = require('url');
// Temptist mail templates
var emails = require('./emails');
// Blacklists
var blacklist = require('../config/blacklist');

var	loadFriendships = function(user) {
	return domain.Friendships.forge().query(function(qb) {
		qb.where('user_id', '=', user.id).orderBy('nickname');
	}).fetch({withRelated:'friend'}).then(function(collection) {
		return domain.toJSON(collection);
	});
};

var loadTemptor = function(user) {
	console.log('user>>>>>>>>>>>>>>>>>>>>',user);
	var g = parseInt(user.get('gender'), 10);
	var a = parseInt(user.get('age_range') ? user.get('age_range') : 0, 10);
	console.log("a >>>>>>>>>>>>>>>>>>>>>>",a);
	console.log("g >>>>>>>>>>>>>>>>>>>>>>",g);
     
	a = isNaN(a) ? 0 : a;
	g = isNaN(g) ? 1 : g;
	return domain.User.where({ is_editor: true, gender: g, age_range: a }).fetch().then(function(fetched) {
		if(fetched)
		{
			console.log("fetched>>>",fetched);
			return fetched.toJSON();
		}
		else{ 
			return domain.User.where({ email: 'guest.' + (g == 0 ? 'm' : 'f') + '@temptist.com' }).fetch().then(function(fallback) {
				if(fallback)
					console.log("fallback>>>>>>>",fallback);
					return fallback.toJSON();
				return Promise.reject(new Error('Guest temptist not found for g:' + g + ' and a:' + a ));
			});
		}
	});
};

var loadEditor = function(id) {
	return domain.User.where({id: id, is_editor: true}).fetch().then(function(user) {
		if(user)
			return user.toJSON();
		return Promise.reject(new Error('Editor'));
	});
};

var loadReciprocalFriends = function(user) {
	return domain.Friendships.forge().query(function(qb) {
		qb.where('friend_id', '=', user.id);
	}).fetch({withRelated:'user'}).then(function(collection) {
		return domain.toJSON(collection);
	});
};

var loadOrCreateUser = function(email) {
	email = email.toLowerCase();
	return domain.User.where({ email : email }).fetch().then(
		function(friend) {
			if(friend) {
				return friend;
			}
			return domain.User.forge({
					email : email,
					has_signed_up: false
				}).save();
			}
	);
};

var createReciprocalFriendship = function (user, friendship_id) {
	return domain.Friendship.where({
		id : friendship_id,
		friend_id : user.id
	}).fetch({withRelated: 'user'}).then(
		function(friendship) {
			if(friendship) {
				domain.Friendship.forge({
					user_id : user.id,
					friend_id : friendship.get('user_id'),
					nickname : friendship.related('user').get('name')
				}).save();
				var sub = user.get('name') + ' has signed-up to temptist';
				var msg = sub + ' Why not check out their temptist.';
				var href = '/temptist/' + fns.toUrl('temptist', user.id, user.get('email'));
				createNotification(friendship.get('user_id'), sub, msg, 5, null, href);
			}
		});
};

var loadFriendship = function(id, user) {
	return domain.Friendship.where({ id : id, user_id : user.id }).fetch({withRelated: ['friend', 'user']}).then(function(friendship) {
		if(friendship)
			return friendship.toJSON();
		return Promise.reject(new Error('Friendship not found'));
	});
};

var loadListingModel = function (id, user) {
	return domain.Listing.where({ id : id, user_id : user.id }).fetch().then(function(listing) {
		if(listing)
			return listing;
		return Promise.reject(new Error('Listing not found'));
	});
};

var createFriendships = function(user, friends) {
	for(var ix = 0; ix < friends.length; ix++) {
		createFriendshipByEmailAndNickname(user, friends[ix].email, friends[ix].nickname);
	}
};

var createFriendshipByEmailAndNickname = function(user, email, nickname) {
	return loadOrCreateUser(email).then(function(friend) {
		return createFriendship(user, friend, nickname);
	});
};

var createFriendshipByEmail = function(user, email) {
	loadOrCreateUser(email).then(function(friend) {
		var nickname = friend.get('name') ? friend.get('name') : friend.get('email'); // TODO should always have a name
		createFriendship(user, friend, nickname);
	});
};

var createFriendship = function(user, friend, nickname) {
	return domain.Friendship.where({
		user_id : user.id,
		friend_id : friend.id
	}).fetch().then(function(friendship) {
		if(!friendship) {
			return domain.Friendship.forge({
				user_id : user.id,
				friend_id : friend.id,
				nickname : nickname
			}).save();
		}
	});
	if(friend.get('has_signed_up')) {
		var msg = user.get('name') + ' just added you as a friend on temptist';
		var href = '/temptist/' + fns.toUrl('temptist', user.id, user.get('email'));
		createNotification(friend.id, msg, msg, 5, null, href);
	}
};

var createReciprocalFriendships = function(user, friendships) {
	for(var ix = 0; ix < friendships.length; ix++) {
		createReciprocalFriendship(user, friendships[ix]);
	}
};

var parseFriendships = function(friendships) {
	var a = { all: friendships, users: [], guests: [] };
	for(var ix = 0; ix < friendships.length; ix++) {
		if(friendships[ix].friend.has_signed_up)
			a.users.push(friendships[ix]);
		else
			a.guests.push(friendships[ix]);
	}
	return a;
};

var createNudge = function(user, type, event, listing, subject, message) {
	return domain.Nudge.forge({
		user_id : user.id,
		nudge_type : type,
		event_id : (event ? event.id : null),
		listing_id : (listing ? listing.id : null),
		subject : subject,
		message : message
	}).save().then(function(saved) {
		return saved.toJSON();
	});
};

var createRecipient = function(nudge, friendship) {
	return domain.Recipient.forge({
		nudge_id : nudge.id,
		friendship_id : friendship.id,
		email : friendship.friend.email
	}).save().then(function(saved) {
		return saved.toJSON();
	});
};

var sendNudgeToFriendship = function(nudge, friendship_id) {
	// console.log(util.inspect(nudge, {showHidden: false, depth: null}));
	loadFriendship(friendship_id, nudge.user).then(function(friendship) {
		createRecipient(nudge, friendship).then(function(recipient) {
			var email = emails.nudge(recipient, nudge.subject, nudge.message, nudge.event, nudge.listing);
			email.sendTo(recipient.email, ! friendship.friend.has_signed_up);
			var href = '/tempt/' + fns.toUrl('nudge', recipient.id, recipient.email);
			createNotification(friendship.friend.id, nudge.subject, nudge.message, nudge.nudge_type, nudge.id, href);
		});
	});
};

var createNotification = function(user_id, subject, message, notification_type, nudge_id, href) {
	return domain.Notification.forge({
		user_id : user_id,
		nudge_id : nudge_id,
		notification_type : notification_type,
		subject : subject,
		message : message,
		href : href,
		has_been_read : false
	}).save().then(function(saved) {
		var notification = saved.toJSON();
		updateNotificationCount(notification.user_id);
		return notification;
	});
};

var getNextOccurs = function(dt) {
	// roll date until it's in the future;
	var td = new Date();
	if(dt.getTime() > td.getTime())
		return dt;
	dt = new Date(Date.UTC(td.getFullYear()-1, dt.getMonth(), dt.getDate(), 0, 0, 0));
	while(td.getTime() >= dt.getTime()) {
		dt = new Date(Date.UTC(dt.getFullYear()+1, dt.getMonth(), dt.getDate(), 0, 0, 0));
	}
	return dt; 
};

var updateNotificationCount = function(user_id) {
	domain.Knex.raw('update users u set notifications = (select count(*) from notifications n where has_been_read = false and n.user_id = u.id) where u.id = ?', [user_id]).then(function(resp) {
		// swallow for now
	});
};

module.exports = {

	publicTemptist: function(user) {
		return domain.Listings.forge().query(function(qb) {
			qb.where('is_removed', '=', false).andWhere('is_private', '=', 'false').andWhere('user_id', '=', user.id).orderBy('id','desc');
		}).fetch({withRelated: 'item'}).then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	privateTemptist: function(user) {
		return domain.Listings.forge().query(function(qb) {
			qb.where('is_removed', '=', false).andWhere('user_id', '=', user.id).orderBy('id','desc');
		}).fetch({withRelated: 'item'}).then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	temptingItemsFor: function(user, temptor) {
		return domain.Listings.forge().query(function(qb) {
			qb.where('is_removed', '=', false).andWhere('is_private', '=', false).andWhere('user_id', '=', temptor.id).orderBy('id','desc');
		}).fetch({withRelated: 'item'}).then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	temptingItemsForGuest: function(user, gender) {
		//console.log("gender>>>>>>>>>>>>>>>>>>>",gender);	    
    		if (gender == 1) {
		return domain.User.where({ email:"guest.f@temptist.com"}).fetch().then(function(fetchedresult) {
			if(fetchedresult){	
			console.log("fetchedresult>>>",fetchedresult.id);
			return domain.Listings.forge().query(function(qb) {
			
			qb.where('is_removed', '=', false).andWhere('is_private', '=', false).andWhere('user_id', '=', fetchedresult.id).orderBy('id','desc');
		}).fetch({withRelated: 'item'}).then(function(collection) {
			return domain.toJSON(collection);
		});
			}
					});   
		}else{
		    return domain.User.where({ email:"guest.m@temptist.com"}).fetch().then(function(fetchedresult) {
			if(fetchedresult){	
			console.log("fetchedresult>>>",fetchedresult.id);
			return domain.Listings.forge().query(function(qb) {
			
			qb.where('is_removed', '=', false).andWhere('is_private', '=', false).andWhere('user_id', '=', fetchedresult.id).orderBy('id','desc');
		}).fetch({withRelated: 'item'}).then(function(collection) {
			return domain.toJSON(collection);
		});
			}
					});   
		    
		}
		
		
			
	},
	loadTemptor: loadTemptor,
	
	loadSuggestions: function(user, ix) {
		return domain.User.where({is_special: true, gender: user.get('gender'), age_range: user.get('age_range')}).fetch().then(function(special) {
			if(special) {
				return Promise.join(
					domain.Listings.forge().query(function(qb) {
							qb.where('is_removed', '=', false).andWhere('user_id', '=', special.id).andWhere('tags', '=', 'question' + ix);
						}).fetch({withRelated: 'item'}),
					domain.Knex('listings').where({ is_removed: false, user_id: special.id, tags: 'question'+(ix+1)}).count('id as CNT'),
					function(listings, total) {
						return { next_count: total[0].CNT, listings: domain.toJSON(listings) };
					});
//				return domain.Listings.forge().query(function(qb) {
//					qb.where('is_removed', '=', false).andWhere('user_id', '=', special.id).andWhere('tags', '=', 'question' + ix);
//				}).fetch({withRelated: 'item'}).then(function(collection) {
//					return domain.toJSON(collection);
//				});
			}
			return Promise.reject(new Error('Suggestion user not found for g:' + user.get('gender') + ' and a:' + user.get('age_range')));
		});
	},
	
	privateItems: function(user) {
		return domain.Knex('listings')
			.where({ user_id: user.id, is_removed: false})
			.select('item_id').then(function(rs) {
				var items = [];
				for(var ix = 0; ix < rs.length; ix++)
					items.push(rs[ix].item_id);
				return items;
			});
	},
	
	loadTemptistUser: function(id) {
		return domain.User.where({id: id, has_signed_up: true, is_locked: false}).fetch().then(function(user) {
			if(user)
				return user.toJSON();
			return Promise.reject(new Error('User not found'));
		});
	},

	updatePassword: function(id, password) {
		return domain.User.where({id: id}).fetch().then(function(user) {
			if(user) {
				user.setPassword(password);
				return user.save();
			}
		});
	},
	
	loadTemptistUserByEmail: function(email) {
		return domain.User.where({email: email.toLowerCase(), has_signed_up: true}).fetch().then(function(user) {
			if(user) {
				if(user.get('is_locked'))
					return Promise.reject(new Error('User is locked'));
				return user.toJSON();
			}
			return Promise.reject(new Error('User not found'));
		});
	},
	
	loadNotifications: function(user) {
		return domain.Notifications.forge().query(function(qb) {
			qb.where('user_id', '=', user.id).orderBy('id','desc');
		}).fetch().then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	loadNotificationsUnread: function(user) {
		return domain.Notifications.forge().query(function(qb) {
			qb.where('user_id', '=', user.id).andWhere('has_been_read', '=', false).orderBy('id','desc');
		}).fetch().then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	readNotification: function(user, notification_id) {
		domain.Knex.raw('update notifications set has_been_read = true where user_id = ? and id = ?', [user.id, notification_id]).then(function(resp) {
			updateNotificationCount(user.id);
		});
	},
	
	readNotifications: function(user) {
		domain.Knex.raw('update notifications set has_been_read = true where user_id = ?', [user.id]).then(function(resp) {
			updateNotificationCount(user.id);
		});
	},

	loadEvents: function (user) {
		return domain.Events.forge().query(function(qb) {
			qb.where('user_id', '=', user.id).orderByRaw('extract(month from occurs), extract(day from occurs)');
		}).fetch({withRelated:'holiday'}).then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	loadHolidays: function () {
		return domain.Holidays.forge().query(function(qb) {
			qb.where('ix', '>', -1).orderBy('ix');
		}).fetch().then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	loadHoliday: function (id) {
		return domain.Holiday.where({ id : id }).fetch().then(function(holiday) {
			if(holiday)
				return holiday.toJSON();
			return Promise.reject(Error('Holiday not found'));
		});
	},
	
	loadOrCreateUser: loadOrCreateUser,
	
	createReciprocalFriendships: createReciprocalFriendships,
	createFriendships: createFriendships,
	createFriendshipByEmailAndNickname: createFriendshipByEmailAndNickname,
	createFriendship: createFriendship,
	
	loadFriendships: loadFriendships,
	
	loadReciprocalFriends: loadReciprocalFriends,
	
	loadFriendshipsForNudge: function(user) {
		return domain.Friendships.forge().query(function(qb) {
			qb.where('user_id', '=', user.id)
				.andWhere('nudge_status', '=', 0).orderBy('nickname');
		}).fetch({withRelated:'friend'}).then(function(collection) {
			return domain.toJSON(collection);
		});
	},
	
	loadFriendsWithTemptists: function(user) {
		return loadFriendships(user).then(parseFriendships).then(function(a) {
			return a.users;
		});
	},
	
	parseFriendships: parseFriendships,
	
	loadPotentialFriends: function(user) {
		return Promise.join(loadFriendships(user), loadReciprocalFriends(user), function(myFriends, meAsFriend) {
			var potentials = [];
			for(var ix = 0; ix < meAsFriend.length; ix++) {
				var isPotential = true;
				for(var jx = 0; jx < myFriends.length; jx++) {
					if(meAsFriend[ix].user_id == myFriends[jx].friend_id) {
						isPotential = false;
						break;
					}
				}
				if(isPotential)
					potentials.push(meAsFriend[ix]);
			};
			return potentials;
		});
	},

	loadFriendship: loadFriendship,
	
	copyListing: function(id, user) {
		return domain.Listing.where({ id : id, is_private : false }).fetch().then(function(listing) {
			if(listing) {
				return domain.Listing.forge({
					url: listing.get('url'),
					img_url: listing.get('img_url'),
					user_id: user.id,
					item_id: listing.get('item_id'),
					title: listing.get('title'),
					tags: '',
					comments: '',
					is_removed: false,
					price_cur: listing.get('price_cur'),
					price_val: listing.get('price_val')
				}).save();
			}
			return Promise.reject(new Error('Listing not found'));
		});
	},
	
	loadListing: function(id, user) {
		return domain.Listing.where({ id : id, user_id : user.id }).fetch().then(function(listing) {
			if(listing)
				return listing.toJSON();
			return Promise.reject(new Error('Listing not found'));
		});
	},

	loadPublicListing: function(id) {
		return domain.Listing.where({ id : id, is_private : false }).fetch().then(function(listing) {
			if(listing)
				return listing.toJSON();
			return Promise.reject(new Error('Listing not found'));
		});
	},
	
	copyPublicListing: function(id, user) {
		return domain.Listing.where({ id : id, is_private : false }).fetch().then(function(listing) {
			if(listing) {
				return domain.Listing.forge({
					url: listing.get('url'),
					img_url: listing.get('img_url'),
					user_id: user.id,
					item_id: listing.get('item_id'),
					title: listing.get('title'),
					tags: '',
					comments: '',
					is_removed: true, // HACK: don't display inchoate listings so set them to be removed BUT don't set the "removed" field as this isn't a user generated action
					price_cur: listing.get('price_cur'),
					price_val: listing.get('price_val')
				}).save().then(function(saved) {
					return saved.toJSON();
				});
			} else
				return Promise.reject(new Error('Listing not found'));
		});
	},
	
	loadEvent: function(id, user) {
		return domain.Event.where({ id : id, user_id: user.id }).fetch({withRelated: 'holiday'}).then(function(event) {
			if(event)
				return event.toJSON();
			return Promise.reject(new Error('Event not found'));
		});
	},

	loadEventForUpdate: function(id, user) {
		return domain.Event.where({ id : id, user_id: user.id }).fetch({withRelated: 'holiday'}).then(function(event) {
			if(event)
				return event;
			return Promise.reject(new Error('Event not found'));
		});
	},

	loadNudge: function(id, user) {
		return domain.Nudge.where({ id : id, user_id: user.id }).fetch({withRelated: ['user', 'event', 'listing'] }).then(function(nudge) {
			if(nudge)
				return nudge.toJSON();
			return Promise.reject(new Error('Nudge not found'));
		});
	},

	addEvent: function(holiday_id, user_id, title, occurs) {
		var oc = new Date(Date.UTC(occurs.getFullYear(), occurs.getMonth(), occurs.getDate(), 0, 0, 0));
		var nt = getNextOccurs(oc);
		return domain.Event.forge({
			holiday_id: holiday_id,
			user_id: user_id,
			title: title,
			occurs: oc,
			next: nt
		}).save();
	},
	
	getNextOccurs: getNextOccurs,
	
	deleteEvent: function(id, user) {
		return domain.Event.where({ id : id, user_id: user.id }).fetch().then(function(event) {
			if(event)
				return event.destroy();
			return Promise.reject(new Error('Event not found'));
		});
	},
	
	deleteListing: function deleteListing(id, user) {
		return domain.Listing.where({ id : id, user_id: user.id }).fetch().then(function(listing) {
			if(listing)
				return listing.destroy();
			return Promise.reject(new Error('Listing not found'));
		});
	},
	
	createReciprocalFriendship: createReciprocalFriendship,
	
	loadOrCreateItem: function(url, user) {
		return domain.Item.where({ url : url }).fetch().then(
			function(item) {
				if(item) {
					var payload = { item: item.toJSON(), user: user };
					return payload;
				}
				return domain.Item.forge({
					url : url,
					user_id: user.id,
					img_url: '/static/images/noimage.png',
					price_cur: 'XXX',
					price_val: 0
				}).save().then(function(saved) {
					var payload = { item: saved.toJSON(), user: user };
					return payload;
				});
			});
	},
	
	createListing: function(payload) {
		return domain.Listing.forge({
			url: payload.item.url,
			img_url: payload.item.img_url,
			user_id: payload.user.id,
			item_id: payload.item.id,
			title: '',
			tags: '',
			comments: '',
			is_removed: true, // HACK: don't display inchoate listings so set them to be removed BUT don't set the "removed" field as this isn't a user generated action
			price_cur: payload.item.price_cur,
			price_val: payload.item.price_val
		}).save();
	},
	
	updateListing: function(id, user, listing) {
		return loadListingModel(id, user).then(function(model) {
			model.set('is_removed', false);
			model.set('title', listing.title);
			model.set('comments', listing.comments);
			model.set('tags', listing.tags);
			model.set('is_favourite', listing.is_favourite);
			model.set('is_private', listing.is_private);
			if(listing.price_val != model.attributes.price_val || listing.price_cur != model.attributes.price_cur) {
				model.set('price_val', listing.price_val);
				model.set('price_cur', listing.price_cur);
				domain.Price.forge({
					price_val: listing.price_val,
					price_cur: listing.price_cur,
					item_id: model.attributes.item_id,
					url: model.attributes.url
				}).save();
			}
			if(listing.img_url) {
				if(listing.img_url == 'remove')
					model.set('img_url', '/static/images/noimage.png');
				else {
					if(model.attributes.img_url != listing.img_url) {
						// TODO cache locally
						model.set('img_url', listing.img_url);
					}
				}
			}
			return model.save().then(function(saved) {
				return saved.toJSON();
			});
		});
	},
	
	updateNudgeStatus: function(id, status) {
		domain.Friendship.where({id : id}).fetch().then(function(friendship) {
			if(friendship) {
				friendship.set('nudge_status', status);
				friendship.save();
			}
		});
	},

	updateOutcome: function(id, status) {
		domain.Recipient.where({id : id}).fetch().then(function(recipient) {
			if(recipient) {
				recipient.set('outcome', status);
				recipient.save();
			}
		});
	},

	verifyEmail: function(user) {
		domain.User.where({id : user.id}).fetch().then(function(usr) {
			if(usr) {
				usr.set('has_verified_email', true);
				usr.save();
			}
		});
	},

	loadUser: function(id) {
		return domain.User.where({ id : id }).fetch().then(function(user) {
			if(user)
				return user.toJSON();
			return Promise.reject(new Error('User not found'));
		})
	},
	
	loadRecipient: function(id) {
		return domain.Recipient.where({ id : id }).fetch({withRelated: ['friendship', 'nudge']}).then(function(recipient) {
			if(recipient)
				return recipient.toJSON();
			return Promise.reject(new Error('Recipient not found'));
		});
	},
	
	sendNudgeToFriends: function(nudge, friendships) {
		for(var ix = 0; ix < friendships.length; ix++) {
			sendNudgeToFriendship(nudge, friendships[ix]);
		}
	},
	
	/* emails */
	emailPasswordReset: function(user) {
		var email = emails.password(user.id, user.email, user.name);
		email.sendTo(user.email);
	},
	
	emailWelcome: function(user) {
		var email = emails.welcome(user.id, user.email, user.name);
		email.sendTo(user.email);
	},
	
	emailContact: function(email, name, feedback) {
		var email = emails.contact(email, name, feedback);
		email.sendTo('team@temptist.com');
	},
	
	/* nudges & notifications
	 * 
	 * 1: invite
	 * 2. hint
	 * 3. tempt
	 * 4. share for event
	 * 5. added you as friend (notification only)
	 * 6. friend signed up (notification only)
	 * 7. smartphone reminder (notification only)
	 *  
	 *  */
	createNudge: createNudge,  /* function(user, type, event, listing, subject, message) { */
	createRecipient: createRecipient,  /* function(nudge, friendship) */
	
	emailInvite: function(friendship, message) {
		createNudge(friendship.user, 1, null, null, friendship.user.name + ' thinks that you would like to try temptist', message).then(function(nudge) {
			createRecipient(nudge, friendship).then(function(recipient) {
				var email = emails.nudge(recipient, nudge.subject, nudge.message, null, null);
				email.sendTo(recipient.email, ! friendship.friend.has_signed_up);
				//createNotification(friendship.friend.id, nudge.subject, nudge.message, nudge.nudge_type, nudge.id );
			});
		});
	},
	
	emailHint: function(listing, friendship, message) {
		createNudge(friendship.user, 2, null, listing, friendship.user.name + ' has dropped a hint', message).then(function(nudge) {
			createRecipient(nudge, friendship).then(function(recipient) {
				var email = emails.nudge(recipient, nudge.subject, nudge.message, listing, null);
				email.sendTo(recipient.email, ! friendship.friend.has_signed_up);
				var href = '/tempt/' + fns.toUrl('nudge', recipient.id, recipient.email);
				createNotification(friendship.friend.id, nudge.subject, nudge.message, nudge.nudge_type, nudge.id, href );
			});
		});
	},

	emailTempt: function(listing, friendship, message) {
		createNudge(friendship.user, 3, null, listing, friendship.user.name + ' thinks you might like this', message).then(function(nudge) {
			createRecipient(nudge, friendship).then(function(recipient) {
				var email = emails.nudge(recipient, nudge.subject, nudge.message, listing, null);
				email.sendTo(recipient.email, ! friendship.friend.has_signed_up);
				var href = '/tempt/' + fns.toUrl('nudge', recipient.id, recipient.email)
				createNotification(friendship.friend.id, nudge.subject, nudge.message, nudge.nudge_type, nudge.id, href );
			});
		});
	},

	/*************************************************************
	 *                                                           *
	 *  Very crude crawler (that doesn't actually 'crawl')       * 
	 *  -------------------------------------------------------  *
	 *  This is designed as a version 0.1 just to try and get    *
	 *  the html from the url and parse out any images and       *
	 *  prices. A simple blacklist is employed to prevent abuse  *
	 *  but a more sophisticated one will be required.           *
	 *                                                           *
	 *************************************************************/
	crawl: function(listing) {
		return request(listing.attributes.url).then(function(contents) {
			//console.log(contents);
			var imgs = [], prices = [], decimals = [], appendTitle = false, title = '', poundNext = false;
			var parser = new htmlparser.Parser({
			    onopentag: function(name, attribs){
			        if(name === "img"){
			        	if(attribs.src)
			        		imgs.push(attribs.src);
			        } else if(name === "title"){
			        	appendTitle = true;
			        } else {
			        	appendTitle = false;
			        }
			    },
			    onclosetag: function(name, attribs){
			        if(name === "title"){
			        	appendTitle = false;
			        }
			    },
			    ontext: function(text){
			    	if(poundNext) {
			    		text = '£' + text;
			    		poundNext = false;
			    	}
			    	if(text.indexOf('£') > -1 )
			    		poundNext = true;
			    		//console.log(text);
			    	// TODO match commas for thousands etc.
			    	// var price = /(\$|£)([1-9][0-9]*|0)(\.[0-9]{2})?/g;
			    	var price = /(\$|£)([1-9][0-9]*|0)(\.[0-9]{2})?/g;
			    	while (match = price.exec(text)) {
			    		poundNext = false;
			    		fns.pushIfAbsent(prices, match[0]);
			        }
			    	var decimal = /([1-9][0-9]*|0)(\.[0-9]{2})/g;
			    	while (match = decimal.exec(text)) {
			    		poundNext = false;
			    		fns.pushIfAbsent(decimals, match[0]);
			        }
			    	if(appendTitle) {
			    		title += text;
			    		//appendTitle = false;
			    	}
			    }
			}, { decodeEntities: true });

			parser.write(contents);
// Cheerio isn't very reliable - stream style htmlparser is better
//			$ = cheerio.load(contents);
//			var imgs = [];
//
//			$('img').each(function(i, elem) {
//				imgs[i] = $(this).attr('src');
//			});
//			
//			for(var ix = 0; ix < imgs.length; ix++) {
//				console.log(imgs[ix]);
//			}
			var baseUri = liburl.parse(listing.attributes.url);
//			console.log(baseUri);
			var deduped = [];
			for(var ix = 0; ix < imgs.length; ix++) {
				var uri = liburl.resolve(baseUri, imgs[ix]);
				if(blacklist.isHostBlacklisted(uri.hostname))
					continue;
				var img = liburl.format(uri);
				if(blacklist.isImageBlacklisted(img))
					continue;
				fns.pushIfAbsent(deduped, img);
			}
//			imgs.forEach(function(img) {
//				img = url.format(url.resolve(baseUri, img));
//			});
			
			return { listing: listing, title: title.replace(/^\s\s*/, '').replace(/\s\s*$/, '').substring(0,75), imgs: deduped, prices: prices, decimals: decimals };
		}).catch(Promise.RejectionError, function (e) {
		    console.error("unable to read file, because: ", e.message);
			return { listing: listing, title: '', imgs: [], prices: [], decimals: [] };
		});
	}
};