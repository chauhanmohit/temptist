module.exports = {
	dest: 'uploads/',
	rename: function (fieldname, filename) {
		return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
	},
	files: 1,
	fileSize: 500000 
};

// Busboy, available options
//
// fieldNameSize - integer - Max field name size (Default: 100 bytes)
// fieldSize - integer - Max field value size (Default: 1MB)
// fields - integer - Max number of non-file fields (Default: Infinity)
// fileSize - integer - For multipart forms, the max file size (in bytes) (Default: Infinity)
// files - integer - For multipart forms, the max number of file fields (Default: Infinity)
// parts - integer - For multipart forms, the max number of parts (fields + files) (Default: Infinity)
// headerPairs - integer - For multipart forms, the max number of header key=>value pairs to parse Default: 2000 (same as node's http).