// Hosts are fully checked, so you must blacklist ALL subdomains of a host

var blacklisthost = [
    'www.temptist.com'
];

// Images are partially checked from the start, so you must check both http and https (if you need) BUT
// it is possible to exclude whole folders of images (like for amazon below).

var blacklistsrc = [
    'http://g-ecx.images-amazon.com/images/G/01/amazonui/sprites',
    'http://g-ecx.images-amazon.com/images/G/02/gno/sprites'
];

var isImageBlacklisted = function(src) {
	for(var jx = 0; jx < blacklistsrc.length; jx++) {
		if(src.indexOf(blacklistsrc[jx]) === 0)
			return true;
		if(blacklistsrc[jx] > src)
			return false;
	}
	return false;
};

var isHostBlacklisted = function(hostname) {
	return blacklisthost.indexOf(hostname) > -1;
}

module.exports = {
	isImageBlacklisted: isImageBlacklisted,
	isHostBlacklisted: isHostBlacklisted
}