// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var RememberMeStrategy = require('passport-remember-me').Strategy;
var configAuth = require('./auth');
var country = require('./country');
//console.log("FacebookStrategy>>>>>>>>>>>>>>>>" , FacebookStrategy);

// load up the user model
var domain = require('../app/model/domain');

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session

	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		domain.User.where({
			id : id
		}).fetch().then(function(model) {
			done(null, model);
		});
	});

	// =========================================================================
	// LOCAL SIGNUP ============================================================
	// =========================================================================
	// we are using named strategies since we have one for login and one for
	// signup (by default, if there was no name, it would just be called 'local')

	passport.use('local-signup', new LocalStrategy({
		// by default, local strategy uses username and password, we will
		// override with email
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback : true
	// allows us to pass back the entire request to the callback
	}, function(req, email, password, done) {
		console.log("welcome>>>>>>>>>>>>>>>>>>>>>>>user>>>>", email, password);
		console.log("usernameField>>>>>>>>>>>>>>>>>>>>>>>user>>>>" ,email);
		console.log("passwordField>>>>>>>>>>>>>>>>>>>>>>>password>>>>" ,password);
		process.nextTick(function() {
			console.log("welcome>>>>>>>>>>>>>>>>>>>>>>>process.nextTick>>>>");
			// validate inputs
			req.checkBody('email', 'A valid email is required').isValidEmail();
			req.checkBody('name', 'Your name is required').len(1, 50);
			req.checkBody('country', 'Please select your country').notEmpty();
			req.checkBody('gender', 'Your gender is required').isInt();
			
			req.sanitize('gender').toInt();
			
			var errors = req.validationErrors();
			
			if(errors) {
				var errorText = '';
				for(var ix = 0; ix < errors.length; ix++)
					errorText += '{param: "' + errors[ix].param +  '", msg: "' + errors[ix].msg +  '", value: "' + errors[ix].value +  '"}, ';
				return done(null, false, req.flash('signupMessage', 'Please correct the errors: ' + errorText));
			}
				
			var ctry = country.codeFromName(req.query['country']);
			console.log("welcome>>>>>>>>>>>>>>>>>>>>>>>ctry>>>>" , ctry);
			var name = req.query['name'];
			var gender = req.query['gender'];
			console.log("welcome>>>>>>>>>>>>>>>>>>>>>>>name>>>>" , name);
			console.log("welcome>>>>>>>>>>>>>>>>>>>>>>>gender>>>>" , gender);
			// find a user whose email is the same as the forms email
			// we are checking to see if the user trying to login already exists
			console.log("email>>>>>>>",email);
			domain.User.where({
				email : email.toLowerCase()
			}).fetch().then(
					function(user) {
						
						console.log("user>>>>user>>>>>>>>>>" , user); console.log("domain.User>>>>>>>>>>" , domain.User);
						if(user) { console.log("user>>>>>>>>>>>>>>");
							if(user.get('has_signed_up')) { console.log("has_signed_up>>>>>>>>>>>>>>");
								
								return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
							} else {
								console.log('dgdfgdfgdfg',name);
								user.set('name', name);
								user.set('country', ctry);
								user.set('gender', gender);
								user.set('age_range', 0);
								user.set('has_signed_up', true);
								user.setPassword(password);
								user.save().then(function(saved) { console.log("saved>>>>>>>>>>>>>>>>" , saved);
									return done(null, saved);
								});
							}
						} else {
							console.log('dgdfgdfgdfg',name);
							console.log('dgdfgdfgdfg',email);
							console.log('dgdfgdfgdfg',ctry);
							console.log('dgdfgdfgdfg',gender);
							user = domain.User.forge({
								email : email,
								name : name,
								country : ctry,
								gender: gender,
								has_signed_up: true
							});
							user.setPassword(password);
							
							user.save().then(function(saved) { console.log("saved>>>>saved>>>>>>>>>>123" , saved);
								return done(null, saved);
							});
						}
					});
		});

	}));

	passport.use('local-login', new LocalStrategy({
		// by default, local strategy uses username and password, we will
		// override with email
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback : true
	// allows us to pass back the entire request to the callback
	}, function(req, email, password, done) { // callback with email and password from our form

		domain.User.where({
			email : email.toLowerCase()
		}).fetch().then(function(model) {
					console.log(email);
					// TODO merge these to single Bad u/n and p/w combo message 
					if (model == undefined)
						return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
					if (!model.authenticate(password))
						return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

					if (!req.body.remember_me) { return done(null, model); }
					
					issueToken(model, function(err, token) {
						
						if (err)
							throw err;
						req.res.cookie('remember_me', token, { path: '/', httpOnly: true, maxAge: 604800000 });
						return done(null, model);
					});
					
				});
	}));

	passport.use(new FacebookStrategy({ 
		// pull in our app id and secret from our auth.js file
		clientID : configAuth.facebookAuth.clientID,
		clientSecret : configAuth.facebookAuth.clientSecret,
		callbackURL : configAuth.facebookAuth.callbackURL,
		passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
	},

	// facebook will send back the token and profile
	function(req, token, refreshToken, profile, done) { console.log("hiiiiiiiiiiiiiiii" , done);

		// asynchronous
		process.nextTick(function() {

			console.log(profile);
			
			domain.Login.where({username: profile.id}).fetch({withRelated: ['user']}).then(function(login) {
				if(login) {
					console.log( { login: login, user: login.related('user') } );
					return done(null, login.related('user'));
				} else {
					login = {
						username: profile.id,
						token: token
					};
					if (req.user) {
						// associate this Login with the authenticated User
						req.user.related('logins').create(login);
						return done(null, req.user);
					} else {
						// Lookup the user using the email address from facebook profile
						// if no email address is returned from facebook then abort
						if(profile.emails.length == 0 || ! profile.emails[0].value)
							return done(null, false);
						var email = profile.emails[0].value.toLowerCase();
						domain.User.where({email: email}).fetch().then(function(user) {
							if(user) {
								user.set('has_signed_up', true);
								if(profile.gender == 'female')
									user.set('gender', 1);
								if(profile.gender == 'male')
									user.set('gender', 0);
								if(!user.attributes.name)
									user.set('name', profile.name.givenName + ' ' + profile.name.familyName);
								return user;
							} else {
								return domain.User.forge({
									email: email,
									name: profile.name.givenName + ' ' + profile.name.familyName,
									gender: profile.gender == 'male' ? 0 : 1,
									age_range: 0,
									has_signed_up: true
								});
							}
						}).then(function(user) {
							user.save().then(function(saved) {
								saved.related('logins').create(login);
								return done(null, saved);
							});
						});
					}
				}
			});
		});

	}));

	passport.use(new RememberMeStrategy(
		function(token, done) {
			consumeRememberMeToken(token, function(err, uid) {
				if (err) { return done(err); }
				if (!uid) { return done(null, false); }
				domain.User.where({
					id : uid
				}).fetch().then(function(user) {
					if (!user) { return done(null, false); }
					return done(null, user);
				});
			});
		}, issueToken));

	function issueToken(user, done) {
		var token = randomString(64);
		saveRememberMeToken(token, user.id, function(err) {
		if (err) { return done(err); }
		return done(null, token);
		});
	}

	var tokens = {}
	function consumeRememberMeToken(token, fn) {
		var uid = tokens[token];
		// 	invalidate the single-use token
		delete tokens[token];
		return fn(null, uid);
	};
	function saveRememberMeToken(token, uid, fn) {
		tokens[token] = uid;
		return fn();
	};
	
	function randomString(len) {
		var buf = []
		, chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
		, charlen = chars.length;
		for (var i = 0; i < len; ++i) {
			buf.push(chars[getRandomInt(0, charlen - 1)]);
		}
		return buf.join('');
	};
	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};
};