// NOTE: apostrophes must be preceeded by a backslash! e.g. ' => \'

var questions = { 
		m0: [
//{ h1: 'Like a good book?', p:'Here are some of our favourites:' },
{ h1: 'You can never have too many gadgets.', p:'These might not be essential but they are certainly cool.' },
//{ h1: 'Handy around the house? Want to be?', p:'Add one of these novel tools to your Temptist.' },
{ h1: 'Turn your animal house into the ultimate man cave.', p:'' },
{ h1: 'You are a man of experience and intellect.', p:'Dress accordingly:' },
{ h1: 'No such thing as too much sport?', p:'' },
{ h1: 'You\'re young and irresistable.', p:'Keep it that way.' },
{ h1: 'Like a good book?', p:'Here are some of our favourites:', p:'' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into your Temptist and keep track of everything in one place?' }
	], 	m1: [
{ h1: 'You can never have too many gadgets.', p:'These might not be essential but they are certainly cool.' },
{ h1: 'Turn your animal house into the ultimate man cave.', p:'' },
{ h1: 'In the mood for some new threads?', p:'' },
{ h1: 'No such thing as too much sport?', p:'' },
{ h1: 'You\'re young and irresistable.', p:'Keep it that way.' },
{ h1: 'Like a good book?', p:'Here are some of our favourites:', p:'' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into your Temptist and keep track of everything in one place?' }
	], 	m2: [
{ h1: 'You can never have too many gadgets.', p:'These might not be essential but they are certainly cool.' },
{ h1: 'Turn your animal house into the ultimate man cave.', p:'' },
{ h1: 'You are a man of experience and intellect.', p:'Dress accordingly:' },
{ h1: 'No such thing as too much sport?', p:'' },
{ h1: 'You\'re young and irresistable.', p:'Keep it that way.' },
{ h1: 'Like a good book?', p:'Here are some of our favourites:', p:'' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into your Temptist and keep track of everything in one place?' }
	], 	m3: [
{ h1: 'You can never have too many gadgets.', p:'These might not be essential but they are certainly cool.' },
{ h1: 'Turn your animal house into the ultimate man cave.', p:'' },
{ h1: 'You are a man of experience and intellect.', p:'Dress accordingly:' },
{ h1: 'No such thing as too much sport?', p:'' },
{ h1: 'You\'re still young and irresistable.', p:'Keep it that way.' },
{ h1: 'Like a good book?', p:'Here are some of our favourites:', p:'' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into your Temptist and keep track of everything in one place?' }
	], 	m4: [
{ h1: 'You can never have too many gadgets.', p:'These might not be essential but they are certainly cool.' },
{ h1: 'Turn your animal house into the ultimate man cave.', p:'' },
{ h1: 'You are a man of experience and intellect.', p:'Dress accordingly:' },
{ h1: 'No such thing as too much sport?', p:'' },
{ h1: 'You\'re still young and irresistable.', p:'Keep it that way.' },
{ h1: 'Like a good book?', p:'Here are some of our favourites:', p:'' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into your Temptist and keep track of everything in one place?' }
	], 	f0: [
{ h1: 'Feel good inside and out with these beauty finds.', p:'' },
{ h1: 'In the mood for some new clothes?', p:'What are we saying, who isn\'t?' },
{ h1: 'Fact: you can never have too many accessories', p:'' },
{ h1: 'Who says you need to be in school to justify beautiful new stationary?', p:'' }, 
{ h1: 'Whether you\'re a domestic goddess or aspire to be one:', p:'These homewares will help release your inner Nigella or Kelly' },
{ h1: 'Strong is sexy.', p:'Use these items to stay fighting fit.' },
//{ h1: 'Not that you need the help but here are some handy kitchen tools that will make cooking a breeze.' },
//{ h1: 'Like a good book?', p:'Here are some of our favourites:' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into temptist.com and keep track of everything in one place?' },
	], 	f1: [
{ h1: 'Feel good inside and out with these beauty finds.', p:'' },
{ h1: 'In the mood for some new clothes?', p:'What are we saying, who isn\'t?' },
{ h1: 'Fact: you can never have too many accessories', p:'' },
{ h1: 'Who says you need to be in school to justify beautiful new stationary?', p:'' }, 
{ h1: 'Whether you\'re a domestic goddess or aspire to be one:', p:'These homewares will help release your inner Nigella or Kelly' },
{ h1: 'Strong is sexy.', p:'Use these items to stay fighting fit.' },
//{ h1: 'Like a good book?', p:'Here are some of our favourites:' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into temptist.com and keep track of everything in one place?' },
	], 	f2: [
{ h1: 'Feel good inside and out with these beauty finds.', p:'' },
{ h1: 'In the mood for some new clothes?', p:'What are we saying, who isn\'t?' },
{ h1: 'Fact: you can never have too many accessories', p:'' },
{ h1: 'Who says you need to be in school to justify beautiful new stationary?', p:'' }, 
{ h1: 'Whether you\'re a domestic goddess or aspire to be one:', p:'These homewares will help release your inner Nigella or Kelly' },
{ h1: 'Strong is sexy.', p:'Use these items to stay fighting fit.' },
//{ h1: 'Like a good book?', p:'Here are some of our favourites:' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into temptist.com and keep track of everything in one place?' },
	], 	f3: [
{ h1: 'Feel good inside and out with these beauty finds.', p:'' },
{ h1: 'In the mood for some new clothes?', p:'What are we saying, who isn\'t?' },
{ h1: 'Fact: you can never have too many accessories', p:'' },
{ h1: 'Who says you need to be in school to justify beautiful new stationary?', p:'' }, 
{ h1: 'Whether you\'re a domestic goddess or aspire to be one:', p:'These homewares will help release your inner Nigella or Kelly' },
{ h1: 'Strong is sexy.', p:'Use these items to stay fighting fit.' },
//{ h1: 'Like a good book?', p:'Here are some of our favourites:' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into temptist.com and keep track of everything in one place?' },
	], 	f4: [
{ h1: 'Feel good inside and out with these beauty finds.', p:'' },
{ h1: 'In the mood for some new clothes?', p:'What are we saying, who isn\'t?' },
{ h1: 'Fact: you can never have too many accessories', p:'' },
{ h1: 'Who says you need to be in school to justify beautiful new stationary?', p:'' }, 
{ h1: 'Whether you\'re a domestic goddess or aspire to be one:', p:'These homewares will help release your inner Nigella or Kelly' },
{ h1: 'Strong is sexy.', p:'Use these items to stay fighting fit.' },
//{ h1: 'Like a good book?', p:'Here are some of our favourites:' },
{ h1: 'Do you have any existing wish and gift lists online?', p:'Why not copy those items into temptist.com and keep track of everything in one place?' },
	] };

module.exports = {
	loadQuestion: function(gender, ix, age) {
		var ar = (age === undefined ) ? 0 : parseInt(age, 10);
		if(parseInt(gender, 10) == 1)
			return questions['f'+ar][ix];
		return questions['m'+ar][ix];
	}
}