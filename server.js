/*************************
 *                       *
 *    T E M P T I S T    * 
 *    ---------------    *
 *    MAIN APP SERVER    *
 *                       *
 *************************/

var db = require('./config/db.js');

var dbTemptist = {
   	    migrationDirectory: __dirname + '/db/temptist', 
   	    driver: db.client,
   	    host: db.connection.host,
   	    port: db.connection.port,
   	    database: db.connection.database,
   	    username: db.connection.user,
   	    password: db.connection.password
   	};
//
//// run db migrations
//var postgrator1 = require('postgrator');
//postgrator1.setConfig(dbTemptist); 
//postgrator1.migrate('001', function (err, migrations) {
//    if (err) {
//        console.log('Postgres DB setup: ' + err);
//    } else { 
//        console.log(migrations);
//    }
//    postgrator1.endConnection(function () {
//    	console.log('Database setup completed and connection closed');
//    	
//    });
//});

// start server
// var server = 

// Exception handling functions
//function logErrors(err, req, res, next) {
//	console.error(err.stack);
//	next(err);
//};
//
//function clientErrorHandler(err, req, res, next) {
//	if (req.xhr) {
//		res.status(500).send({ error: 'Something blew up!' });
//	} else {
//		next(err);
//	}
//};
//
//function errorHandler(err, req, res, next) {
//	res.status(500);
//	res.render('common/error', { error: err });
//};

// cluster
var 
	cluster			= require('cluster'),
	express			= require('express'),
	session      	= require('express-session'),
	fs 				= require('fs'),
    filename 		= '/var/nodelist';

var cacheNodes = [], ClusterStore, MemcachedStore;

fs.readFile(filename, 'UTF8', function(err, data) {
    if (!err && data) {
        var lines = data.split('\n');
        for (var i = 0 ; i < lines.length ; i++) {
            if (lines[i].length > 0) {
            	console.log('Cache: ' + lines[i]);
                cacheNodes.push(lines[i]);
            }
        }
    } else {
    	console.log('FS error: ' + err);
    }
    if(cacheNodes && cacheNodes.length > 0)
    	MemcachedStore  = require('connect-memcached')(session);
    else
    	ClusterStore 	= require('strong-cluster-connect-store')(session);
   
    console.log('NODE_ENV: ' + process.env.NODE_ENV);
    console.log('PARAM1: ' + process.env.PARAM1);
    console.log('PARAM2: ' + process.env.PARAM2);
    console.log('PARAM3: ' + process.env.PARAM3);
    console.log('PARAM4: ' + process.env.PARAM4);
    console.log('PARAM5: ' + process.env.PARAM5);
    
	var workers = {}, count = require('os').cpus().length;
	
	function spawn() {
		  var worker = cluster.fork();
		  workers[worker.pid] = worker;
		  return worker;
	};
	
	if (cluster.isMaster) {
	  // In real life, you'd probably use more than just 2 workers,
	  // and perhaps not put the master and worker in the same file.
	  //
	  // You can also of course get a bit fancier about logging, and
	  // implement whatever custom logic you need to prevent DoS
	  // attacks and other bad behavior.
	  //
	  // See the options in the cluster documentation.
	  //
	  // The important thing is that the master does very little,
	  // increasing our resilience to unexpected errors.
	
		if(!cacheNodes)
			ClusterStore.setup();
		
		for (var i = 0; i < count; i++) {
			spawn();
		}
	
		cluster.on('disconnect', function(worker) {
			console.log('worker ' + worker.pid + ' died. spawning a new process...');
		    delete workers[worker.pid];
		    spawn();
		});
	  
	  var postgrator2 = require('postgrator');
		postgrator2.setConfig(dbTemptist); 
		postgrator2.migrate('002', function (err, migrations) {
		    if (err) {
		        console.log('Temptist DB setup: ' + err);
		    } else { 
		        console.log(migrations);
		    }
		    postgrator2.endConnection(function () {
		    	console.log('Database migrations completed and connection closed');
		    });
		});
	
	} else {
	  // the worker
	  //
	  // This is where we put our bugs!
	
		
		var	port     		= process.env.PORT || 4001,
			passport 		= require('passport'),
			flash 	 		= require('connect-flash'),
			morgan       	= require('morgan'),
			multer       	= require('multer'),
			cookieParser 	= require('cookie-parser'),
			bodyParser   	= require('body-parser'),
			//	sanitizer       = require('express-sanitizer'),
			validator 		= require('express-validator'),
			db				= require('./config/db.js'),
			fns				= require('./app/fns.js'),
			helmet			= require('helmet'),
			util 			= require('util');
	
		require('./config/passport.js')(passport); // pass passport for configuration
	
		var multerOpts = require('./config/multer.js');
		
		var app = express();
	
		app.use(require('express-domain-middleware'));
		
		if(cacheNodes && cacheNodes.length) {
			console.log('Using MemcachedStore for session sharing: ' + cacheNodes);
			app.use(session({
	            secret: 'VrR3QEHSPMDqx9bnfkmcpeaeGGFQLbpV',
	            store: new MemcachedStore({'hosts':cacheNodes})
	        }));
		} else {
			console.log('Using ClusterStore for session sharing');
			app.use(session({ store: new ClusterStore(), secret: 'VrR3QEHSPMDqx9bnfkmcpeaeGGFQLbpV' }));
		}
	
		app.use(function errorHandler(err, req, res, next) {
			  console.error(err.stack);
			  console.log('error on request %d %s %s: %j', process.domain.id, req.method, req.url, err);
			  res.send(500, "Sorry, your request could not be processed. Please try again.");
			  if(err.domain) {
			    //you should think about gracefully stopping & respawning your server 
			    //since an unhandled error might put your application into an unknown state 
				  try {
				        // make sure we close down within 30 seconds
				        var killtimer = setTimeout(function() {
				          process.exit(1);
				        }, 30000);
				        // But don't keep the process open just for that!
				        killtimer.unref();
	
				        // stop taking new requests.
				        app.close();
	
				        // Let the master know we're dead.  This will trigger a
				        // 'disconnect' in the cluster master, and then it will fork
				        // a new worker.
				        cluster.worker.disconnect();
				  } catch (er2) {
				        // oh well, not much we can do at this point.
				        console.error('Error sending 500!', er2.stack);
				  }
			  }
		});
		
		if(process.env.NODE_ENV == 'production') {
			app.use(function(req, res, next) {
			    if((!req.secure) && (req.get('X-Forwarded-Proto') !== 'https')) { // assumes webserver/loadbalancer adds header
			        res.redirect('https://' + req.get('Host') + req.url);
			    }
			    else
			        next();
			});
			app.use(morgan('common', { skip: function(req, res) { return res.statusCode < 400 } }));
		} else {
			app.use(morgan('dev')); // log every request to the console			
		}
		
		app.use("/static", express.static(__dirname + '/static'));
		app.use("/upload", express.static(__dirname + '/uploads'));
		app.use(helmet());
		app.use(cookieParser()); // read cookies (needed for auth)
		app.use(bodyParser()); // get information from html forms
		//app.use(sanitizer()); // this line must be immediately after express.bodyParser()!
		app.use(validator({
			customValidators: {
			    isValidEmail: function(value) {
			        return fns.isEmail(value);
			    }
			 }
		}));
		app.use(multer(multerOpts).single('uploaded_img')); // file uploads (well, anything that uses multipart/form)
	//	app.use(session({ secret: 'VrR3QEHSPMDqx9bnfkmcpeaeGGFQLbpV' })); // session secret
		app.use(passport.initialize());
		app.use(passport.session()); // persistent login sessions
		app.use(passport.authenticate('remember-me'));
		app.use(flash()); // use connect-flash for flash messages stored in session
	//	app.use(logErrors);
	//	app.use(clientErrorHandler);
	//	app.use(errorHandler);
		app.set('view engine', 'ejs'); // set up ejs for templating
	
		app.locals.fns = fns;
		
		// routes ======================================================================
		require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
	
		app.listen(port, function() {
		    console.log('Listening on port %d', port);
		    console.log(util.inspect(dbTemptist, {showHidden: false, depth: null}));
		});
	}

});